import 'react-native-gesture-handler';
import 'react-native-permissions';
import {Navigation} from 'react-native-navigation';
import {onAppLaunched as _onAppLaunched} from "./src/App";
import _registerComponents from "./src/navigation/registerComponents";
import {appearComponent, disappearComponent} from "./src/navigation/navigationActions";
import reduxStore from "./src/reduxStore";
console.disableYellowBox = true;
function dispatch(action) {
    if (reduxStore && reduxStore.dispatch) {
        reduxStore.dispatch(action);
    }
}
_registerComponents();
Navigation.events().registerComponentDidAppearListener((params) => {
    dispatch(appearComponent(params));
});

Navigation.events().registerComponentDidDisappearListener((params) => {
    dispatch(disappearComponent(params));
});

Navigation.events().registerAppLaunchedListener(() => {
    _onAppLaunched();
});
