// simple
type FontWeightType = "normal" | "bold" | "200" | "300" | "400" | "500" | "600" | "700";
type FontType = "title"
    | "description"
    | "body"
    | "caption"
    | "h1"
    | "h2"
    | "h3"
    | "h4"
    | "h5"
    | "h6";
type CommonTextColorType = "primary" | "secondary" | "reversePrimary" | "reverseSecondary" | "disable";
type CommonThreeColorType = "light" | "regular" | "dark";

// generic
type Fonts<T> = {
    [key in FontType]: T
};
type CommonTextColors<T> = {
    [key in CommonTextColorType]: T
};
type CommonThreeColors<T> = {
    [key in CommonThreeColorType]: T
};

// instance
export interface FontSizes extends Fonts<number> {

}

export interface FontWeights extends Fonts<FontWeightType> {

}

export interface FontColors extends CommonTextColors<string> {

}

export interface PrimaryColors extends CommonThreeColors<string> {

}

export interface SecondaryColors extends CommonThreeColors<string> {

}

export interface BorderColors extends CommonThreeColors<string>{

}

export interface ThemeProps {
    fontSizes: FontSizes,
    fontWeight: FontWeights,
    fontColors: FontColors,
    primaryColors: PrimaryColors,
    secondaryColors: SecondaryColors,
    borderColors: BorderColors,
}
