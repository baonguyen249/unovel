import {ThemeProps} from "./themeTypes";
import {borderColors, fontSizes, fontWeights, primaryColors, secondaryColors} from "../consts";
import {
    TEXT_BLACK_DISABLE,
    TEXT_BLACK_PRIMARY,
    TEXT_BLACK_SECONDARY,
    TEXT_WHITE_PRIMARY, TEXT_WHITE_SECONDARY
} from "js-kit/consts";

export const lightTheme: ThemeProps = {
    fontColors: {
        primary: TEXT_BLACK_PRIMARY,
        secondary: TEXT_BLACK_SECONDARY,
        disable: TEXT_BLACK_DISABLE,
        reversePrimary: TEXT_WHITE_PRIMARY,
        reverseSecondary: TEXT_WHITE_SECONDARY,
    },
    fontSizes: fontSizes,
    fontWeight: fontWeights,
    primaryColors: primaryColors.lightTheme,
    secondaryColors: secondaryColors.lightTheme,
    borderColors: borderColors.lightTheme,
};
