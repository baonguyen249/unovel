import React from "react";
const defaultValue: any = {};
const AppTheme = React.createContext(defaultValue);
export default AppTheme;
