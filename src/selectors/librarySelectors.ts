import {LibraryState} from "../types";
import {LIBRARY_STATE_KEY} from "../consts";

export const selectLibraryState = (state: any) => {
    if (state) {
        return  state[LIBRARY_STATE_KEY];
    }
};

export const selectBooks = (state: any) => {
    const libraryState: LibraryState = selectLibraryState(state);
    if (libraryState) {
        const {
            books,
        } = libraryState;
        return books;
    }
};
