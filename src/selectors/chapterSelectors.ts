import {ChapterState} from "../types";
import {CHAPTER_STATE_KEY} from "../consts";

export const selectChapterState = (state: any) => {
    if (state)  {
        return state[CHAPTER_STATE_KEY];
    }
};

export const selectChapterInfos = (state: any) => {
    const chapterState: ChapterState = selectChapterState(state);
    if (chapterState) {
        const {
            chapterInfos,
        } = chapterState;
        return chapterInfos;
    }
};
