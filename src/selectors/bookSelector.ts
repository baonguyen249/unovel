import {BookState} from "../types";
import {BOOK_STATE_KEY} from "../consts";

export const selectBookState = (state: any) => {
    if (state) {
        return state[BOOK_STATE_KEY];
    }
};

export const selectBookInfos = (state: any) => {
    const bookState: BookState = selectBookState(state);
    if (bookState) {
        const {
            bookInfos,
        } = bookState;
        return bookInfos;
    }
};
