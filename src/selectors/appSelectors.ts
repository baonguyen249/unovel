import {AppState} from "../types/clientModels";
import {APP_STATE_KEY} from "../consts";

export const selectAppState = (state: any) => {
    if (state) {
        return state[APP_STATE_KEY];
    }
};

export const selectCurrentAppState = (state: any) => {
    const appState: AppState = selectAppState(state);
    if (appState) {
        const {
            currentAppState,
        } = appState;
        return currentAppState;
    }
};

export const selectLastActiveTime = (state: any) => {
    const appState: AppState = selectAppState(state);
    if (appState) {
        const {
            lastActiveTime,
        } = appState;
        return lastActiveTime;
    }
};

export const selectLastBackgroundTime = (state: any) => {
    const appState: AppState = selectAppState(state);
    if (appState) {
        const {
            lastBackgroundTime,
        } = appState;
        return lastBackgroundTime;
    }
};
