import {PERMISSION_UNDETERMINED} from "../consts";
import {PermissionState} from "../types/clientModels";

export const selectPermissionState = (state: any) => {
    if (state) {
        const {
            permissionState,
        } = state;
        return permissionState;
    }
    return null;
};

export const selectLocationPermission = (state: any) => {
    const permissionState: PermissionState = selectPermissionState(state);
    if (permissionState) {
        const {
            location
        } = permissionState;
        return location || PERMISSION_UNDETERMINED;
    }
    return PERMISSION_UNDETERMINED;
};

export const selectNotificationPermission = (state: any) => {
    const permissionState: PermissionState = selectPermissionState(state);
    if (permissionState) {
        const {
            notification
        } = permissionState;
        return notification || PERMISSION_UNDETERMINED;
    }
    return PERMISSION_UNDETERMINED;
};

export const selectContactPermission = (state: any) => {
    const permissionState: PermissionState = selectPermissionState(state);
    if (permissionState) {
        const {
            contacts
        } = permissionState;
        return contacts || PERMISSION_UNDETERMINED;
    }
    return PERMISSION_UNDETERMINED;
};
export const selectCameraPermission = (state: any) => {
    const permissionState: PermissionState = selectPermissionState(state);
    if (permissionState) {
        const {
            camera
        } = permissionState;
        return camera || PERMISSION_UNDETERMINED;
    }
    return PERMISSION_UNDETERMINED;
};
export const selectMicrophonePermission = (state: any) => {
    const permissionState: PermissionState = selectPermissionState(state);
    if (permissionState) {
        const {
            microphone
        } = permissionState;
        return microphone || PERMISSION_UNDETERMINED;
    }
    return PERMISSION_UNDETERMINED;
};

export function selectPhotoPermission(state: any) {
    const permissionState: PermissionState = selectPermissionState(state);
    if (permissionState) {
        const {
            photos,
        } = permissionState;
        return photos || PERMISSION_UNDETERMINED;
    }
    return PERMISSION_UNDETERMINED;
}
