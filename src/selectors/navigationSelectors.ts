import {NavigationState} from "../types";
import {NAVIGATION_STATE_KEY} from "../consts";

function selectNavigationState(state: any) {
    if (state) {
        return state[NAVIGATION_STATE_KEY];
    }
}

export function selectCurrentPageName(state: any) {
    const navigationState: NavigationState = selectNavigationState(state);
    if (navigationState) {
        const {
            currentPageName,
        } = navigationState;
        return currentPageName;
    }
}

export function selectCurrentPageId(state: any) {
    const navigationState: NavigationState = selectNavigationState(state);
    if (navigationState) {
        const {
            currentPageId,
        } = navigationState;
        return currentPageId;
    }
}

export function selectCurrentModalName(state: any) {
    const navigationState: NavigationState = selectNavigationState(state);
    if (navigationState) {
        const {
            currentModalName,
        } = navigationState;
        return currentModalName;
    }
}

export function selectCurrentModalId(state: any) {
    const navigationState: NavigationState = selectNavigationState(state);
    if (navigationState) {
        const {
            currentModalId,

        } = navigationState;
        return currentModalId;
    }
}

export function selectComponentInfos(state: any) {
    const navigationState: NavigationState = selectNavigationState(state);
    if (navigationState) {
        const {
            componentInfos,
        } = navigationState;
        return componentInfos;
    }
}

export function selectCurrentComponentId(state: any) {
    const navigationState: NavigationState = selectNavigationState(state);
    if (navigationState) {
        const {
            currentComponentId,
        } = navigationState;
        return currentComponentId;
    }
}

export function selectCurrentComponentName(state: any) {
    const navigationState: NavigationState = selectNavigationState(state);
    if (navigationState) {
        const {
            currentComponentName,
        } = navigationState;
        return currentComponentName;
    }
}

export function selectCurrentOverlayId(state: any) {
    const navigationState: NavigationState = selectNavigationState(state);
    if (navigationState) {
        const {
            currentOverlayId,
        } = navigationState;
        return currentOverlayId;
    }
}

export function selectCurrentOverlayName(state: any) {
    const navigationState: NavigationState = selectNavigationState(state);
    if (navigationState) {
        const {
            currentOverlayName,
        } = navigationState;
        return currentOverlayName;
    }
}
