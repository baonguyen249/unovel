const addPrefix = (type: string) => {
    return "APP_ACTIONS/" + type;
};
export const CHANGE_APP_STATE = addPrefix("CHANGE_APP_STATE");

