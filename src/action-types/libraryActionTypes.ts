const addPrefix = (actionType: string) => {
    return "LIBRARY_ACTIONS/" + actionType;
};

export const UPDATE_BOOKS_TO_LIBRARY = addPrefix("UPDATE_BOOKS_TO_LIBRARY");