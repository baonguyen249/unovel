const addPrefix = (type: string) => {
    return "CHAPTER_ACTIONS/" + type;
};

export const UPDATE_CHAPTER_INFOS = addPrefix("UPDATE_BOOK_INFOS");
export const DELETE_CHAPTER_INFOS = addPrefix("DELETE_BOOK_INFOS");
