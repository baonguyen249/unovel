const addPrefix = (type: string) => {
    return "BOOK_ACTIONS/" + type;
};

export const UPDATE_BOOK_INFOS = addPrefix("UPDATE_BOOK_INFOS");
export const DELETE_BOOK_INFOS = addPrefix("DELETE_BOOK_INFOS");
