import {persistStore} from "redux-persist";
import reduxStore from "./reduxStore";
import {changeAppState, onAppStart} from "./actions/appActions";
import {AppState, AppStateStatus} from "react-native";

function dispatch(action: any | undefined): void {
    if (reduxStore && reduxStore.dispatch) {
        reduxStore.dispatch(action);
    }
}

let reduxPersistor: any;
export const onAppLaunched = () => {
    reduxPersistor = persistStore(reduxStore, {}, () => {
        _registerCommonListeners();
        dispatch(onAppStart());
    });
};

const _registerCommonListeners = () => {
    AppState.addEventListener("change", _appStateChangeListener);
};

function _appStateChangeListener(nextState: AppStateStatus): void {
    dispatch(changeAppState(nextState));
}
export const reduxFlush = () => {
    if (reduxPersistor) {
        reduxPersistor.flush();
    }
};

class App {

}

export default App;


