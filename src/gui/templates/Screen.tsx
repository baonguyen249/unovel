import React from "react";
import {View, StyleSheet} from "react-native";
import {connect} from "react-redux";
import BaseScreen from "../pages/BaseScreen";
import {ScreenProps} from "../../types/commonProps";

interface Props extends ScreenProps<GetStateProps, DispatchProps> {

}

interface State {

}

function getOwnerProps(props: Props) {
    const {
        getStateProps = {} as GetStateProps,
        dispatchProps = {} as DispatchProps,
    } = props;
    return {
        ...props,
        getStateProps,
        dispatchProps,
    };
}

class Screen extends BaseScreen<Props, State> {
    render() {
        return (
            <View style={styles.container}>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});

interface GetStateProps {

}

function getStateToProps(state: any): GetStateProps {
    return {};
}

interface DispatchProps {

}

function dispatchToProps(dispatch: any): DispatchProps {
    return {};
}

function mergeProps(getStateProps: GetStateProps, dispatchProps: DispatchProps, ownerProps: Props): Props {
    return {
        ...ownerProps,
        getStateProps: getStateProps,
        dispatchProps: dispatchProps,
    };
}

export default connect(getStateToProps, dispatchToProps, mergeProps)(Screen);
