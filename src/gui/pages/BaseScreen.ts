import React from "react";
import {BackHandler, StatusBar, StatusBarStyle, StatusBarAnimation} from "react-native";
import {Navigation} from "react-native-navigation";
import shallowCompare from "react-addons-shallow-compare";
import Device from "js-kit/Device";

interface DefaultProps {
    componentId: string,

    [key: string]: any,
}

export default class BaseScreen<Props extends DefaultProps, State> extends React.Component<Props, State> {
    static currentPageProps?: any;
    static pageInstance?: any;

    private _isMountedBS?: boolean;
    _backHandler: any;
    _navigationEventHandler: any;

    private statusBarStyle: StatusBarStyle = "default";
    private isTranslucentStatusBar: boolean = false;
    private statusBarBackgroundColor: string = "white";
    private statusBarAnimation: StatusBarAnimation = "slide";
    private isStatusBarHidden: boolean = false;
    private _isBSAppear = false;

    constructor(props: Props) {
        super(props);
        this.onDidAppear = this.onDidAppear.bind(this);
        this.onDidDisappear = this.onDidDisappear.bind(this);
        this.onDidMount = this.onDidMount.bind(this);
        this.onWillUnmount = this.onWillUnmount.bind(this);
        this.onDidUpdate = this.onDidUpdate.bind(this);

        /**
         * status bar
         * */
        this.setStatusBarHidden = this.setStatusBarHidden.bind(this);
        this.setStatusBarStyle = this.setStatusBarStyle.bind(this);
        this.setStatusBarAnimation = this.setStatusBarAnimation.bind(this);
        this.setStatusBarBackgroundColor = this.setStatusBarBackgroundColor.bind(this);
        this.setTranslucentStatusBar = this.setTranslucentStatusBar.bind(this);
        this.onAndroidBack = this.onAndroidBack.bind(this);
        /**
         * private methods
         * */
    }

    componentDidMount() {
        this._navigationEventHandler = Navigation.events().bindComponent(this);
        this._isMountedBS = true;
        this.onDidMount();
    }

    componentWillUnmount() {
        if (this._navigationEventHandler) {
            this._navigationEventHandler.remove();
        }
        this._isMountedBS = false;
        this.onWillUnmount();
    }

    /**
     * Optimization performance
     * Limit re-render
     * */
    shouldComponentUpdate(nextProps: any, nextState: any, nextContext: any): any {
        if (!BaseScreen.currentPageProps) {
            return true;
        }
        const {
            componentId
        } = nextProps;
        if (BaseScreen.currentPageProps.componentId === componentId) {
            const compare = shallowCompare(this, nextProps, nextState);
            return compare;
        }
        return false;
    }

    componentDidUpdate(prevProps: any, prevState: any, snapshot: any) {
        const {
            componentId,
        } = this.props;
        /**
         * Update Props
         * */
        if (BaseScreen.currentPageProps) {
            if (BaseScreen.currentPageProps.componentId === componentId) {
                BaseScreen.currentPageProps = {
                    ...this.props
                };
            }
        }

        this.onDidUpdate(prevProps, prevState, snapshot);
    }

    componentDidAppear() {
        this._isBSAppear = true;
        const {
            componentId,
        } = this.props;
        this._backHandler = BackHandler.addEventListener("hardwareBackPress", this.onAndroidBack);
        this.onDidAppear();
        BaseScreen.currentPageProps = {
            ...this.props
        };
        BaseScreen.pageInstance = this;

        StatusBar.setHidden(this.isStatusBarHidden, this.statusBarAnimation);

        if (!this.isStatusBarHidden) {
            /**
             * set statusBar DarkMode Just Ios
             * Android handled with react-native-navigation
             */
            const statusBarStyle: StatusBarStyle = "light-content";
            Device.isIos && StatusBar.setBarStyle(statusBarStyle || this.statusBarStyle, true);
        }
        this.forceUpdate();
        /* requestAnimationFrame(() => {
         });*/
    }

    componentDidDisappear() {
        this._isBSAppear = false;
        const {
            componentId,
        } = this.props;
        if (this._backHandler) {
            this._backHandler.remove();
        }
        this.onDidDisappear();
        this.forceUpdate();
        /*requestAnimationFrame(() => {
        });*/
    }

    /**
     *
     * */

    onDidMount() {

    }

    onDidUpdate(prevProps: any, prevState: any, snapshot: any) {

    }

    onWillUnmount() {

    }

    onDidAppear() {

    }

    onDidDisappear() {

    }

    protected setStatusBarHidden(hidden: boolean = false): void {
        this.isStatusBarHidden = hidden;
    }

    /**
     * slide, fade, none
     * */
    protected setStatusBarAnimation(animation: StatusBarAnimation = "slide") {
        this.statusBarAnimation = animation;
    }

    /**
     * default, light-content, dark-content
     * */
    protected setStatusBarStyle(statusBarStyle: StatusBarStyle = "default"): void {
        this.statusBarStyle = statusBarStyle;
    }

    /**
     * Android only
     * */
    protected setTranslucentStatusBar(isTranslucent: boolean = false): void {
        this.isTranslucentStatusBar = isTranslucent;
    }

    protected setStatusBarBackgroundColor(backgroundColor: string = ""): void {
        this.statusBarBackgroundColor = backgroundColor;
    }

    protected onAndroidBack() {
        return false;
    }

    protected isAppear = () => {
        return this._isBSAppear;
    };
}
