import React from "react";
import {View, StyleSheet, FlatList} from "react-native";
import {connect} from "react-redux";
import BaseScreen from "../pages/BaseScreen";
import {ScreenProps} from "../../types/commonProps";
import {selectBooks} from "../../selectors/librarySelectors";
import memoize from "memoize-one";
import {Book} from "../../types/coreModels";
import PageWrapper from "../components/wrappers/PageWrapper";
import {FLEX_1} from "js-kit/consts";
import BodyText from "../components/texts/BodyText";

interface Props extends ScreenProps<GetStateProps, DispatchProps> {

}

interface State {

}

function getOwnerProps(props: Props) {
    const {
        getStateProps,
        dispatchProps,
    } = props;
    return {
        ...props,
        getStateProps: getStateProps || {
            books: undefined,
        },
        dispatchProps: dispatchProps || {},
    };
}

class LibraryPage extends BaseScreen<Props, State> {
    _uniqueId = "LIBRARY:" + Date.now();
    _memoBooks = memoize((books) => {
        if (books && books.size > 0) {
            return books.map((book: Book) => {
                if (book) {
                    const {
                        updatedTimeMs,
                        name,
                        author,
                        uniqueId,
                    } = book;
                    return {
                        updatedTimeMs,
                        name,
                        author,
                        uniqueId,
                    };
                }
            }).sort((l: any, r: any) => {
                if (l && r) {
                    const {
                        updatedTimeMs: lUpdatedTimeMs,
                    } = l;
                    const {
                        updatedTimeMs: rUpdatedTimeMs,
                    } = r;
                    if (lUpdatedTimeMs < rUpdatedTimeMs) {
                        return 1;
                    }
                    if (lUpdatedTimeMs > rUpdatedTimeMs) {
                        return -1;
                    }
                }
                return 0;
            }).toIndexedSeq().toArray();
        }
        return [];
    });

    _getData = () => {
        const {
            getStateProps,
        } = getOwnerProps(this.props);
        const {
            books,
        } = getStateProps;
        return this._memoBooks(books);
    };

    render() {
        const data = this._getData();
        console.log("data:", data);
        return (
            <PageWrapper>
                <View style={styles.container}>
                    <FlatList
                        style={FLEX_1}
                        contentContainerStyle={[styles.contentContainer]}
                        data={data}
                        renderItem={this._renderItem}
                        keyExtractor={this._keyExtractor}
                        pinchGestureEnabled={false}
                        showsVerticalScrollIndicator={false}
                        nestedScrollEnabled={false}
                        numColumns={2}
                    />
                </View>
            </PageWrapper>
        );
    }

    _keyExtractor = (item: any, index: number) => {
        return this._uniqueId + ":item:" + index;
    };
    _renderItem = ({item}: any) => {
        let authorName = "";
        let bookName = "";
        if (item) {
            const {
                name,
                author,
            } = item;
            bookName = name;
            authorName = author.name;
        }
        return (
            <View style={styles.item}>
                <BodyText>
                    {bookName}
                </BodyText>
                <BodyText>
                    {authorName}
                </BodyText>
            </View>
        );
    };
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    contentContainer: {
        alignItems: "stretch",
    },
    item: {
        flex: 1,
        height: 100,
        margin: 5,
    },
});

interface GetStateProps {
    books?: any,
}

function getStateToProps(state: any): GetStateProps {
    const books = selectBooks(state);
    return {
        books,
    };
}

interface DispatchProps {

}

function dispatchToProps(dispatch: any): DispatchProps {
    return {};
}

function mergeProps(getStateProps: GetStateProps, dispatchProps: DispatchProps, ownerProps: Props): Props {
    return {
        ...ownerProps,
        getStateProps: getStateProps,
        dispatchProps: dispatchProps,
    };
}

export default connect(getStateToProps, dispatchToProps, mergeProps)(LibraryPage);
