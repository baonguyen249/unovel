import React from "react";
import {View, StyleSheet} from "react-native";
import {connect} from "react-redux";
import BaseScreen from "../pages/BaseScreen";
import {ScreenProps} from "../../types/commonProps";
import PageWrapper from "../components/wrappers/PageWrapper";
import ChapterList from "../components/chapter/ChapterList";
import ItemWrapper from "../components/wrappers/ItemWrapper";
import ChapterRow from "../components/chapter/ChapterRow";
import {selectChapterInfos} from "../../selectors/chapterSelectors";
import {selectBookInfos} from "../../selectors/bookSelector";
import {openChapter, popStack} from "../../navigation/navigationActions";

interface Props extends ScreenProps<GetStateProps, DispatchProps> {
    bookId: string,
    onChapterPress?: any,
}

interface State {

}

function getOwnerProps(props: Props) {
    const {
        getStateProps,
        dispatchProps,
    } = props;
    return {
        ...props,
        getStateProps: getStateProps || {
            bookInfo: undefined,
        },
        dispatchProps: dispatchProps || {
            pop: () => {
            }
        },
    };
}

class ChapterListPage extends BaseScreen<Props, State> {
    render() {
        const {
            getStateProps,
        } = getOwnerProps(this.props);
        const {
            bookInfo,
        } = getStateProps;
        return (
            <PageWrapper>
                <ChapterList
                    ItemWrapper={ItemWrapper}
                    itemComponents={[{regNums: [0], Component: ChapterRow}]}
                    bookInfo={bookInfo}
                    regNum={0}
                    refreshing={false}
                    onRefresh={this._onRefresh}
                    onLoadNext={this._onLoadNext}
                    onChapterPress={this._onChapterPress}
                />
            </PageWrapper>
        );
    }

    _onLoadNext = () => {

    };
    _onRefresh = () => {

    };
    _onChapterPress = (chapterId: string) => {
        const {
            dispatchProps,
            onChapterPress,
        } = getOwnerProps(this.props);
        const {
            pop,
        } = dispatchProps;
        onChapterPress && onChapterPress(chapterId);
        pop();
    };
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});

interface GetStateProps {
    bookInfo: any,
}

function getStateToProps(state: any, props: Props): GetStateProps {
    const {
        bookId,
    } = getOwnerProps(props);
    const bookInfos = selectBookInfos(state);
    let bookInfo: any;
    if (bookInfos) {
        bookInfo = bookInfos.get(bookId);
    }
    return {
        bookInfo,
    };
}

interface DispatchProps {
    pop: () => void,
}

function dispatchToProps(dispatch: any): DispatchProps {
    return {
        pop: () => {
            dispatch(popStack());
        }
    };
}

function mergeProps(getStateProps: GetStateProps, dispatchProps: DispatchProps, ownerProps: Props): Props {
    return {
        ...ownerProps,
        getStateProps: getStateProps,
        dispatchProps: dispatchProps,
    };
}

export default connect(getStateToProps, dispatchToProps, mergeProps)(ChapterListPage);
