import React from "react";
import {View, StyleSheet, FlatList, Text, TouchableOpacity} from "react-native";
import {connect} from "react-redux";
import BaseScreen from "../pages/BaseScreen";
import {ChapterListProps, ScreenProps} from "../../types/commonProps";
import _ from "lodash";
import rnTextSize, {} from "react-native-text-size"
import Device from "js-kit/Device";
import {selectChapterInfos} from "../../selectors/chapterSelectors";
import {Chapter} from "../../types/coreModels";
import {FLEX_1, STATUS_BAR_HEIGHT} from "js-kit/consts";
import PageWrapper from "../components/wrappers/PageWrapper";
import {openChapter, openChapterList as _openChapterList} from "../../navigation/navigationActions";
import TopBarLeftPart from "../components/nav/TopBarLeftPart";
import BackIcon from "../components/icons/BackIcon";
import {TOP_BAR_HEIGHT} from "../../consts/dimens";
import BodyText from "../components/texts/BodyText";
import {SHADOW_DEPTH_1} from "native-kit/consts/styles";
import {AbsoluteTopBar} from "js-kit/components";
import {isBlank} from "js-kit/utils";

interface Props extends ScreenProps<GetStateProps, DispatchProps> {
    chapterId: string,
}

interface State {
    width: number,
    height: number,
    data: any[],
    fontSize: number,
    fontWeight: any,
    textPaddingHorizontal: number,
    textPaddingVertical: number,
    letterSpacing: number,
    chapterId?: string,
}

function getOwnerProps(props: Props) {
    const {
        getStateProps,
        dispatchProps,
    } = props;
    return {
        ...props,
        getStateProps: getStateProps || {
            chapterInfos: undefined,
        },
        dispatchProps: dispatchProps || {
            openChapterList: () => {
            }
        },
    };
}

let inst: any;
const END_PAGE = "end";
const _LAYOUT_RATIO = [1, 2, 1];

class ChapterPage extends BaseScreen<Props, State> {
    static updateChap() {
        if (inst && inst.updateChapContext) {
            inst.updateChapContext();
        }
    }

    _uniqueId = "ChapterPage:" + Date.now();
    _listRef: any = React.createRef();
    state: State = {
        width: Device.getCurrentScreenWidth(),
        height: Device.getCurrentScreenHeight(),
        data: [],
        fontSize: 16,
        fontWeight: "400",
        textPaddingHorizontal: 0,
        textPaddingVertical: 0,
        letterSpacing: 0,
    };

    constructor(props: Props) {
        super(props);
        inst = this;
        const {
            chapterId,
        } = getOwnerProps(props);
        this.state.chapterId = chapterId;
    }

    updateChapContext = async (nextChapterId?: string) => {
        const {
            height,
            width,
            chapterId,
        } = this.state;
        const chapterInfo = this._getChapterInfo(nextChapterId);
        console.log("updateChapContext:", chapterId);
        if (!chapterInfo) {
            return;
        }
        const {
            body,
            id,
            title,
        }: Chapter = chapterInfo;
        let contents: string[] = [];
        if (body) {
            contents = _.split(body, "\n\n");
        }
        const fontSize = 20;
        const fontWeight = "400";
        const paddingHorizontal = 10;
        const paddingVertical = 5;
        const letterSpacing = 0;

        const heights = await rnTextSize.flatHeights({
            fontSize,
            fontWeight,
            text: contents,
            letterSpacing,
            width: width - paddingHorizontal * 2,
        });
        const screenHeight = height;
        console.log("dimens:", {width, height});
        let pages: any[] = [];
        let tempPageTexts: string[] = [];
        let tempH = 0;
        const contentLength = contents.length;
        heights.forEach((height, index) => {
            tempH += (height + paddingVertical * 2);
            if (tempH < screenHeight) {
                tempPageTexts.push(contents[index]);
                if (contentLength - 1 === index) {
                    pages.push({contents: tempPageTexts});
                    tempPageTexts = [];
                    tempH = 0;
                }
            } else {
                pages.push({contents: tempPageTexts});
                tempPageTexts = [contents[index]];
                tempH = height;
            }
        });
        pages.push({type: END_PAGE})
        console.log("data:", pages);
        this.setState({
            data: pages,
            fontSize,
            fontWeight,
            textPaddingHorizontal: paddingHorizontal,
            textPaddingVertical: paddingVertical,
            letterSpacing,
            chapterId: nextChapterId || chapterId,
        });
    };

    scrollToOffset = (props: { offset: number, animated: boolean }) => {
        if (this._listRef.current && this._listRef.current.scrollToOffset) {
            this._listRef.current.scrollToOffset(props);
        }
    };

    scrollToTop = (animated = false) => {
        this.scrollToOffset({
            animated,
            offset: 0,
        });
    };

    _getChapterInfo = (nextChapterId?: string) => {
        const {
            getStateProps,
        } = getOwnerProps(this.props);
        const {
            chapterId,
        } = this.state;
        const {
            chapterInfos,
        } = getStateProps;
        if (chapterInfos) {
            const chapterInfo = chapterInfos.get(nextChapterId || chapterId);
            return chapterInfo;
        }
    };

    render() {
        const {
            data,
            chapterId,
        } = this.state;
        console.log("RENDER:", chapterId);
        console.log("data:", data);
        return (
            <PageWrapper>
                <View style={styles.container} onLayout={this._onLayout}>
                    <FlatList
                        ref={this._listRef}
                        style={FLEX_1}
                        contentContainerStyle={[styles.contentContainer]}
                        data={data}
                        renderItem={this.renderItem}
                        keyExtractor={this.keyExtractor}
                        pinchGestureEnabled={false}
                        showsVerticalScrollIndicator={false}
                        nestedScrollEnabled={false}
                        horizontal={true}
                        pagingEnabled={true}
                        onScroll={this._onScroll}
                    />
                </View>
                <AbsoluteTopBar
                    style={[{backgroundColor: "white"}, SHADOW_DEPTH_1]}
                    topSafeHeight={STATUS_BAR_HEIGHT}
                    barHeight={TOP_BAR_HEIGHT}
                    layoutRatio={_LAYOUT_RATIO}
                    renderItem={this._renderTopBarItem}
                />
            </PageWrapper>
        );
    }

    keyExtractor = (item: any, index: number) => {
        return this._uniqueId + index;
    };

    onDidMount() {
        this.updateChapContext();
    }

    onDidUpdate(prevProps: any, prevState: any, snapshot: any) {
        const {
            chapterId,
        } = this.state;
        console.log("onDidUpdate:", chapterId);
    }

    renderItem = ({item, index}: any) => {
        const {
            width,
            height,
            fontWeight,
            fontSize,
            textPaddingHorizontal,
            textPaddingVertical,
            letterSpacing,
        } = this.state;
        const chapterInfo = this._getChapterInfo();
        let textList: any;
        if (item) {
            const {
                contents,
                type,
            } = item;
            if (contents && contents.length > 0) {
                const baseKey = "item" + index;
                textList = contents.map((text: string, index: number) => {
                    return (
                        <Text style={{
                            fontWeight,
                            fontSize: fontSize,
                            paddingHorizontal: textPaddingHorizontal,
                            paddingVertical: textPaddingVertical,
                            letterSpacing: letterSpacing,
                        }} key={baseKey + ":" + index}>
                            {text}
                        </Text>
                    );
                });
            }
            if (type === END_PAGE) {
                let nextChapterId = "";
                if (chapterInfo) {
                    const {
                        nextId,
                    } = chapterInfo;
                    nextChapterId = nextId;
                }
                return (
                    <TouchableOpacity style={[styles.endPage, {width, height}]}
                                      onPress={() => this._onOpenChapter(nextChapterId)}
                    >
                        <BodyText>
                            {"Next Chap"}
                        </BodyText>
                    </TouchableOpacity>
                );
            }
        }
        return (
            <View style={{
                width,
                height,
                backgroundColor: "white",
            }}>
                {textList}
            </View>
        );
    };

    _renderTopBarItem = (index: number) => {
        return <View/>;
    };

    _LeftPart = () => {
        return (
            <TopBarLeftPart Wrapper={TouchableOpacity} Icon={BackIcon}/>
        );
    };

    _MidPart = () => {
        const chapterInfo = this._getChapterInfo();
        let title = "";
        if (chapterInfo) {
            const {
                title: _title,
            } = chapterInfo;
            if (!isBlank(_title)) {
                title = _title;
            }
        }
        return (
            <TouchableOpacity
                style={styles.midPart}
                onPress={this._onOpenChapterList}
            >
                <BodyText numberOfLines={1}>
                    {title}
                </BodyText>
            </TouchableOpacity>
        );
    };

    _RightPart = () => {
        return (
            <View>

            </View>
        );
    };

    _onLayout = (event: any) => {
        const {
            width,
            height,
        } = event.nativeEvent.layout;
        if (width > 0 && height > 0 && (width !== this.state.width || height !== this.state.height)) {
            console.log("_onLayout:", {
                width,
                height,
            });
            this.setState({
                width,
                height,
            }, () => {
                this.updateChapContext();
            });
        }
    };

    _onScroll = (event: any) => {
        // console.log("onScroll:", event.nativeEvent);
    };

    _onOpenChapterList = () => {
        const {
            dispatchProps,
        } = getOwnerProps(this.props);
        const {
            openChapterList,
        } = dispatchProps;
        const chapterInfo = this._getChapterInfo();
        if (chapterInfo) {
            const {
                bookId,
            } = chapterInfo;
            openChapterList({bookId, onChapterPress: this._onOpenChapter});
        }
    };
    _onOpenChapter = (chapterId: string) => {
        console.log("chapterId:", chapterId);
        if (chapterId && typeof chapterId === "string" && chapterId.length > 0 && this.state.chapterId !== chapterId) {
            // force update next chapter
            this.updateChapContext(chapterId).then(() => {
                this.scrollToTop();
            });
        }
    };
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    contentContainer: {
        alignItems: "center",
    },
    midPart: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },
    endPage: {
        justifyContent: "center",
        alignItems: "center",
    },
});

interface GetStateProps {
    chapterInfos?: any,
}

function getStateToProps(state: any, props: Props): GetStateProps {
    const chapterInfos = selectChapterInfos(state);
    return {
        chapterInfos,
    };
}

interface DispatchProps {
    openChapterList: (props: ChapterListProps) => void,
}

function dispatchToProps(dispatch: any): DispatchProps {
    return {
        openChapterList: (props) => {
            dispatch(_openChapterList(props));
        },
    };
}

function mergeProps(getStateProps: GetStateProps, dispatchProps: DispatchProps, ownerProps: Props): Props {
    return {
        ...ownerProps,
        getStateProps: getStateProps,
        dispatchProps: dispatchProps,
    };
}

export default connect(getStateToProps, dispatchToProps, mergeProps)(ChapterPage);
