import React from "react";
import {
    View,
    StyleSheet, TouchableOpacity,
    Text,
    FlatList, TouchableOpacityProps,
} from "react-native";
import {connect} from "react-redux";
import {ScreenProps} from "../../types/commonProps";
import PageWrapper from "../components/wrappers/PageWrapper";
import {
    getBookFromTTV as _getBookFromTTV,
    createBookFromFragments as _createBookFromFragments, getBookListFromJson as _getBookListFromJson,
} from "../../actions/bookActions";
import BaseScreen from "./BaseScreen";
import {openLibrary} from "../../navigation/navigationActions";
import Device from "js-kit/Device";
import {randomColor} from "js-kit/utils";
import {PADDING_H, PADDING_V} from "js-kit/consts";

interface Props extends ScreenProps<GetStateProps, DispatchProps> {

}

interface State {
    width: number,
    height: number,
}

const getOwnerProps = (props: Props) => {
    const {
        getStateProps,
        dispatchProps,
    } = props;
    return {
        ...props,
        getStateProps: getStateProps || {},
        dispatchProps: dispatchProps || {
            getBookFromTTV: () => {
            },
            getBookListFromJson: () => {
            },
            openBookList: () => {
            },
            createBook: () => {
            },
        }
    };
};

interface ButtonProps {
    Wrapper: React.ComponentType<TouchableOpacityProps>,
    text: string,
    onPress?: any,
    textColor?: string,
    buttonColor?: string,
}

const Button = (props: ButtonProps) => {
    const {
        onPress,
        text,
        textColor,
        Wrapper,
        buttonColor,
    } = props;
    return (
        <Wrapper
            onPress={onPress}
            style={[styles.button, {backgroundColor: buttonColor}]}
        >
            <Text
                style={{color: textColor, fontSize: 16, fontWeight: "200"}}
            >
                {text}
            </Text>
        </Wrapper>
    );
};

class HomePage extends BaseScreen<Props, State> {
    _uniqueId = "HomePage:" + Date.now();
    _colors: any = {};
    state: State = {
        width: Device.getCurrentScreenWidth(),
        height: Device.getCurrentScreenHeight(),
    };

    _getColor = (id: string) => {
        const cColor = this._colors[id];
        if (!cColor) {
            this._colors[id] = randomColor();
        }
        return this._colors[id];
    };

    _getDispatchProps = () => {
        const {
            dispatchProps,
        } = getOwnerProps(this.props);
        return dispatchProps;
    };

    render() {
        const {
            componentId,
            dispatchProps,
            getStateProps,
            navigation,
        } = getOwnerProps(this.props);
        return (
            <PageWrapper>
                <View style={styles.container}>
                    <Button
                        Wrapper={TouchableOpacity}
                        onPress={this._onGetBook}
                        text={"Get Content From WEB"}
                        buttonColor={"#e97554"}
                        textColor={"#000000"}
                    />
                </View>
                <View style={styles.container}>
                    <Button
                        Wrapper={TouchableOpacity}
                        onPress={this._createBook}
                        text={"CreateBook"}
                        buttonColor={"#b247ff"}
                        textColor={"#e8e8e8"}
                    />
                </View>
                <View style={styles.container}>
                    <Button
                        Wrapper={TouchableOpacity}
                        onPress={this._onGetBookFromJSON}
                        text={"Get Book From JSON"}
                        buttonColor={"#3eb621"}
                        textColor={"#e8e8e8"}
                    />
                </View>
                <View style={styles.container}>
                    <Button
                        Wrapper={TouchableOpacity}
                        onPress={this._openBookList}
                        text={"Open Books List"}
                        buttonColor={"#218cff"}
                        textColor={"#000000"}
                    />
                </View>
            </PageWrapper>
        );
    }

    _onGetBook = () => {
        const {
            dispatchProps,
        } = getOwnerProps(this.props);
        const {
            getBookFromTTV,
        } = dispatchProps;
        getBookFromTTV();
    };

    _onGetBookFromJSON = () => {
        const {
            dispatchProps,
        } = getOwnerProps(this.props);
        const {
            getBookListFromJson,
        } = dispatchProps;
        getBookListFromJson();
    };
    _createBook = () => {
        const {
            dispatchProps,
        } = getOwnerProps(this.props);
        const {
            createBook,
        } = dispatchProps;
        createBook();
    };

    _openBookList = () => {
        const {
            openBookList,
        } = this._getDispatchProps();
        openBookList();
    };

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },
    contentContainer: {
        alignItems: "center",
    },
    button: {
        paddingVertical: PADDING_V,
        paddingHorizontal: PADDING_H,
    },
});

interface GetStateProps {

}

function getStateToProps(state: any): GetStateProps {
    return {};
}

interface DispatchProps {
    getBookFromTTV: () => void,
    getBookListFromJson: () => void,
    openBookList: () => void,
    createBook: () => void,
}

function dispatchToProps(dispatch: any): DispatchProps {
    return {
        getBookFromTTV: () => {
            dispatch(_getBookFromTTV());
        },
        getBookListFromJson: () => {
            dispatch(_getBookListFromJson());
        },
        openBookList: () => {
            dispatch(openLibrary());
        },
        createBook: () => {
            dispatch(_createBookFromFragments());
        },
    };
}

function mergeProps(getStateProps: GetStateProps, dispatchProps: DispatchProps, ownerProps: Props): Props {
    return {
        ...ownerProps,
        getStateProps: getStateProps,
        dispatchProps: dispatchProps,
    };
}

export default connect(getStateToProps, dispatchToProps, mergeProps)(HomePage);
