import React from "react";
import {
    Text, TextProps,
} from "react-native";
import {ComponentProps} from "../../../types/commonProps";
import {TextStyleType} from "../../../types/styleTypes";
import AppTheme from "../../../theme/AppTheme";
import {ThemeProps} from "../../../theme/themeTypes";

interface Props extends ComponentProps<TextStyleType>, TextProps {

}

const BodyText = (props: Props) => {
    if (props) {
        const {
            style,
            children,
            ...restProps
        } = props;
        return (
            <AppTheme.Consumer children={({fontColors}: ThemeProps) => {
                return (
                    <Text
                        {...restProps}
                        style={[{color: fontColors.primary}, style]}
                    >
                        {children}
                    </Text>
                );
            }}/>
        );
    }
    return <Text/>;
};
export default BodyText;
