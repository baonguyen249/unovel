import React from "react";
import {StyleSheet, View} from "react-native";
import {IconProps} from "js-kit/types";
import {PADDING_H} from "js-kit/consts";

interface Props {
    Wrapper: React.ComponentType<any>,
    Icon: React.ComponentType<IconProps>,
    iconColor?: string,
    onPress?: any,
}

const TopBarLeftPart = (props: Props) => {
    if (props) {
        const {
            Icon,
            Wrapper,
            iconColor = "black",
            onPress,
        } = props;
        return (
            <View style={styles.leftPart}>
                <Wrapper
                    onPress={onPress}
                >
                    <Icon
                        size={20}
                        color={iconColor}
                    />
                </Wrapper>
            </View>
        );
    }
    return <View/>;
};

const styles = StyleSheet.create({
    leftPart: {
        flex: 1,
        justifyContent: "center",
        alignItems: "flex-start",
        paddingLeft: PADDING_H,
    }
});

export default TopBarLeftPart;
