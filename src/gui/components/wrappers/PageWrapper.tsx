import React, {PureComponent} from "react";
import {View, StyleSheet} from "react-native";
import {ComponentProps} from "../../../types/commonProps";
import AppTheme from "../../../theme/AppTheme";
import {lightTheme} from "../../../theme/themes";
import {STATUS_BAR_HEIGHT, TOP_BAR_HEIGHT} from "../../../consts/dimens";

interface Props extends ComponentProps {
    hiddenTopSafeView?: boolean,
}

interface State {

}

const getOwnerProps = (props: Props) => {
    const {
        hiddenTopSafeView = false,
    } = props;
    return {
        ...props,
        hiddenTopSafeView,
    };
};

export default class PageWrapper extends PureComponent<Props, State> {
    render() {
        const {
            children,
            hiddenTopSafeView,
        } = getOwnerProps(this.props);
        return (
            <AppTheme.Provider value={lightTheme}>
                <View style={hiddenTopSafeView ? styles.container : styles.topSafeContainer}>
                    {children}
                </View>
            </AppTheme.Provider>
        );
    }
}

const styles = StyleSheet.create({
    topSafeContainer: {
        flex: 1,
        paddingTop: TOP_BAR_HEIGHT + STATUS_BAR_HEIGHT,
    },
    container: {
        flex: 1,
    },
});
