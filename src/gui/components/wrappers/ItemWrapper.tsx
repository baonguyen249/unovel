import React, {PureComponent} from "react";
import {View, StyleSheet} from "react-native";
import {ChapterItemProps, ChapterItemWrapperProps, ComponentProps} from "../../../types/commonProps";
import _ from "lodash";

interface Props extends ComponentProps, ChapterItemWrapperProps {

}

interface State {

}

const getOwnerProps = (props: Props) => {
    const {} = props;
    return {
        ...props,
    };
};

class ItemWrapper extends PureComponent<Props, State> {
    render() {
        const {
            itemComponents,
            regNum,
            ...restProps
        } = getOwnerProps(this.props);
        if (itemComponents && itemComponents.length > 0) {
            let stop = false;
            let SelectedComponent: any;
            if (itemComponents[0]) {
                const {
                    Component,
                } = itemComponents[0];
                SelectedComponent = Component;
            }
            itemComponents.forEach((item, index) => {
                if (item) {
                    const {
                        regNums,
                        Component,
                    } = item;
                    const hasNum = _.includes(regNums, regNum);
                    if (hasNum && !stop) {
                        stop = true;
                        SelectedComponent = Component;
                    }
                }
            });

            return (
                <SelectedComponent
                    {...restProps}
                    regNum={regNum}
                />
            );
        }
        return (
            <View/>
        );
    }
}

const styles = StyleSheet.create({
    container: {},
});

export default ItemWrapper;
