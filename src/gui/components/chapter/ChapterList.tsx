import React, {PureComponent} from "react";
import {View, StyleSheet, FlatList} from "react-native";
import {ChapterItemProps, ChapterItemWrapperProps, ComponentProps} from "../../../types/commonProps";
import {Book} from "../../../types/coreModels";
import {ItemComponentType} from "../../../types/commonTypes";
import {FLEX_1} from "js-kit/consts";
import SeparatorHeight from "../separators/SeparatorHeight";

interface Props extends ComponentProps {
    onRefresh?: any,
    onLoadNext?: any,
    refreshing?: boolean,
    bookInfo: Book,
    onChapterPress?: any,
    ItemWrapper: React.ComponentType<ChapterItemWrapperProps>,
    itemComponents: ItemComponentType<ChapterItemProps>[],
    regNum?: number,
}

interface State {

}

const getOwnerProps = (props: Props) => {
    const {
        regNum = 0,
    } = props;
    return {
        ...props,
        regNum,
    };
};

class ChapterList extends PureComponent<Props, State> {
    _uniqueId = "ChapterList:" + Date.now();

    render() {
        const {
            bookInfo,
        } = getOwnerProps(this.props);
        let data: any[] = [];
        if (bookInfo) {
            const {
                chapters,
            } = bookInfo;
            if (chapters && chapters.length > 0) {
                data = chapters;
            }
        }
        return (
            <View style={styles.container}>
                <FlatList
                    style={FLEX_1}
                    contentContainerStyle={[styles.contentContainer]}
                    data={data}
                    renderItem={this._renderItem}
                    keyExtractor={this._keyExtractor}
                    ItemSeparatorComponent={this._ItemSeparatorComponent}
                    pinchGestureEnabled={false}
                    showsVerticalScrollIndicator={false}
                    nestedScrollEnabled={false}
                />
            </View>
        );
    }

    _keyExtractor = (item: any, index: number) => {
        return this._uniqueId + index;
    };

    _renderItem = (props: any) => {
        const {
            itemComponents,
            ItemWrapper,
            onChapterPress,
            regNum,
        } = getOwnerProps(this.props);
        return (
            <ItemWrapper
                index={props.index}
                item={props.item}
                onPress={onChapterPress}
                itemComponents={itemComponents}
                regNum={regNum}
            />
        );
    };

    _ItemSeparatorComponent = () => {
        return (
            <SeparatorHeight innerVisible={true}/>
        );
    };
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    contentContainer: {
        alignItems: "stretch",
    },
});

export default ChapterList;
