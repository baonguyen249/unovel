import React from "react";
import {
    StyleSheet,
    TouchableOpacity,
    View
} from "react-native";
import {ChapterItemProps} from "../../../types/commonProps";
import BodyText from "../texts/BodyText";
import {isBlank} from "js-kit/utils";
import {PADDING_H, PADDING_V} from "js-kit/consts";

interface Props extends ChapterItemProps {

}

const ChapterRow = (props: Props) => {
    if (props) {
        const {
            regNum,
            onPress,
            item,
            index,
        } = props;
        let title = "";
        let id = "";
        if (item) {
            const {
                title: _title,
                id: _id,
            } = item;
            if (!isBlank(_title)) {
                title = _title;
            }
            if (_id) {
                id = _id;
            }
        }
        const _onPress = () => {
            onPress && onPress(id);
        };
        return (
            <TouchableOpacity
                style={styles.container}
                onPress={_onPress}
            >
                <BodyText numberOfLines={1}>
                    {title}
                </BodyText>
            </TouchableOpacity>
        );
    }
    return (
        <View>

        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: PADDING_H,
        paddingVertical: PADDING_V,
    },
});

export default ChapterRow;
