import React, {PureComponent} from "react";
import {View, StyleSheet} from "react-native";
import {ComponentProps} from "../../../types/commonProps";
import {MARGIN_H} from "js-kit/consts";
import AppTheme from "../../../theme/AppTheme";
import {ThemeProps} from "../../../theme/themeTypes";

interface Props extends ComponentProps {
    half?: boolean,
    innerVisible?: boolean,
}

interface State {

}

function getOwnerProps(props: Props) {
    const {
        innerVisible = false,
        half = false,
        ...restProps
    } = props;
    return {
        ...restProps,
        half,
        innerVisible,
    };
}

export default class SeparatorHeight extends PureComponent<Props, State> {
    render() {
        const {
            half,
            innerVisible,
        } = getOwnerProps(this.props);
        let innerBorder: any;
        if (innerVisible) {
            innerBorder = (
                <AppTheme.Consumer children={({borderColors}: ThemeProps) => {
                    return (
                        <View style={{
                            backgroundColor: borderColors.light,
                            height: 0.5,
                            marginHorizontal: MARGIN_H,
                        }}/>
                    );
                }}/>
            );
        }
        return (
            <View style={half ? styles.halfContainer : styles.container}>
                {innerBorder}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        height: MARGIN_H,
        alignItems: "stretch",
        justifyContent: "center",
    },
    halfContainer: {
        height: MARGIN_H / 2,
        alignItems: "stretch",
        justifyContent: "center",
    },
});
