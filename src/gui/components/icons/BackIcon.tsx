import React from "react";
import {View} from "react-native";
import Icon from "react-native-vector-icons/AntDesign";

interface Props {
    size?: number,
    color?: string,
    outline?: boolean,
}

const BackIcon = (props: Props) => {
    if (props) {
        const {
            color = "black",
            outline = false,
            size = 20,
        } = props;
        return (
            <Icon color={color} size={size} name={"left"}/>
        );
    }
    return <View/>
};

export default BackIcon;
