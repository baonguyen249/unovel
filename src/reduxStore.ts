import {applyMiddleware, compose, createStore} from "redux";
import {createMigrate, persistCombineReducers} from "redux-persist";
import thunkMiddleware from 'redux-thunk';
import AsyncStorage from "@react-native-community/async-storage";
import {
    APP_STATE_KEY,
    BOOK_STATE_KEY,
    CHAPTER_STATE_KEY,
    LIBRARY_STATE_KEY,
    NAVIGATION_STATE_KEY
} from "./consts";
// reducers
import appReducer from "./reducers/appReducer";
import navigationReducer from "./navigation/navigationReducer";
import bookReducer from "./reducers/bookReducer";
import chapterReducer from "./reducers/chapterReducer";
import reduxTransformer from "./reducers/reduxTransformer";
import libraryReducer from "./reducers/libraryReducer";

const migrations = {};
const config = {
    key: 'primary',
    version: 1,
    transforms: [reduxTransformer],
    whitelist: [APP_STATE_KEY],
    storage: AsyncStorage,
    migrate: createMigrate(migrations, {debug: false}),
    throttle: 2000,
};

const allReducers = {
    [APP_STATE_KEY]: appReducer,
    [NAVIGATION_STATE_KEY]: navigationReducer,
    [BOOK_STATE_KEY]: bookReducer,
    [CHAPTER_STATE_KEY]: chapterReducer,
    [LIBRARY_STATE_KEY]: libraryReducer,
};
//@ts-ignore TS2345
const mobileReducers = persistCombineReducers(config, allReducers);

function initStore() {
    if (__DEV__) {
        return createStore(mobileReducers,
            undefined,
            compose(applyMiddleware(
                thunkMiddleware,
                // logger,
            ))
        );
    }
    return createStore(mobileReducers,
        undefined,
        compose(applyMiddleware(
            thunkMiddleware,
        ))
    );
}

const reduxStore = initStore();

export default reduxStore;
