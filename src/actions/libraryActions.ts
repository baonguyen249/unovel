import {UPDATE_BOOKS_TO_LIBRARY} from "../action-types/libraryActionTypes";
import {Book, Chapter} from "../types";
import {createBookId, createChapterId} from "../utils";
import {updateChapterInfos} from "./chapterActions";

interface UpdateBooksProps {
    books: any[],
    refreshed?: boolean,
}

export const updateBooks = (props: UpdateBooksProps) => {
    return (dispatch: any, getState: any) => {
        if (!props) {
            return;
        }
        const {
            books,
            refreshed,
        } = props;
        if (books && books.length > 0) {
            let bookArray: any[] = [];
            books.forEach((book) => {
                if (book) {
                    const {
                        createdTimeMs = 0,
                        id: bookId,
                        chapters = [],
                        author,
                        description,
                        name,
                    } = book;
                    const uniqueBookId = createBookId(bookId, createdTimeMs);
                    const chapterMaxInfos: any[] = [];
                    const chapterMinInfos: any[] = [];
                    let currentIndex = 0;
                    if (chapters && chapters.length > 0) {
                        chapters.forEach((chapter: any) => {
                            if (chapter) {
                                const {
                                    chapterNumber,
                                    createdTimeMs: createdTimeMsOfChapter,
                                    body,
                                    title,
                                    id: chapterId,
                                } = chapter;
                                chapterMinInfos.push({
                                    chapterNumber,
                                    createdTimeMs: createdTimeMsOfChapter,
                                    index: currentIndex,
                                });
                                const uniqueChapterId = createChapterId(bookId, createdTimeMsOfChapter, chapterNumber);
                                chapterMaxInfos.push({
                                    uniqueId: uniqueChapterId,
                                    chapterNumber,
                                    createdTimeMs: createdTimeMsOfChapter,
                                    body,
                                    title,
                                    bookId,
                                    id: chapterId,
                                } as Chapter);
                                currentIndex++;
                            }
                        });
                        dispatch(updateChapterInfos(chapterMaxInfos));
                    }
                    bookArray.push({
                        uniqueId: uniqueBookId,
                        id: bookId,
                        chapters: chapterMinInfos,
                        author,
                        description,
                        name,
                        createdTimeMs,
                        updatedTimeMs: Date.now(),
                    } as Book);
                }
            });
            dispatch(_updateBooksToLibrary(bookArray));
            console.log("Done");
        }
    }
};

const _updateBooksToLibrary = (books: any[], refreshed?: boolean) => {
    return {
        type: UPDATE_BOOKS_TO_LIBRARY,
        books,
        refreshed,
    };
};
