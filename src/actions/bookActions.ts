import {UPDATE_BOOK_INFOS} from "../action-types/bookActionTypes";
import {getBookFromTTV as _getBookFromTTVApi} from "../apis/bookApi";
import {updateChapterInfos} from "./chapterActions";
import {createFile, getJsonContent, mergeBook, readFiles, validateBook} from "../utils";
import _ from "lodash";
import {updateBooks} from "./libraryActions";

const URL = "https://truyen.tangthuvien.vn/doc-truyen/dichkiem-nghich-thuong-khung-suu-tam";

export const updateBookInfos = (books: any[]) => {
    return {
        type: UPDATE_BOOK_INFOS,
        books,
    };
};

export const getBookFromTTV = () => {
    return async (dispatch: any) => {
        try {
            const result = await _getBookFromTTVApi(URL);
            console.log({result});
            if (result) {
                const {
                    chapters,
                } = result;
                console.log("result:", result);
                createFile("kntk1501-1896", "json", JSON.stringify(result));
                // dispatch(updateBookInfos([result]));
                // if (chapters && chapters.length > 0) {
                //     dispatch(updateChapterInfos(chapters));
                // }
            }
        } catch (e) {
            console.log(e);
        }
    }
};

export const getBookListFromJson = () => {
    return async (dispatch: any) => {
        try {
            const files = await readFiles("json");
            let books: any[] = [];
            let promises: Promise<any>[] = [];
            if (files && files.length > 0) {
                files.forEach((file: any) => {
                    if (file) {
                        const {
                            path,
                        } = file;
                        console.log("file:", file);
                        if (path) {
                            promises.push(getJsonContent(path));
                        }
                    }
                });
            }
            const jsons = await Promise.all(promises);
            if (jsons.length > 0) {
                jsons.forEach((json: any) => {
                    const jsonContent = JSON.parse(json);
                    if (validateBook(jsonContent)) {
                        books.push(jsonContent)
                    }
                });
            }
            if (books.length > 0) {
                dispatch(updateBooks({books}));
            }
        } catch (e) {

        }
    }
};

export const createBookFromFragments = () => {
    return async (dispatch: any) => {
        try {
            const jsonFiles = await readFiles("json");
            let promises: Promise<any>[] = [];
            if (jsonFiles.length > 0) {
                jsonFiles.forEach((file: any) => {
                    if (file) {
                        const {
                            path,
                        } = file;
                        console.log("file:", file);
                        if (path) {
                            promises.push(getJsonContent(path));
                        }
                    }
                });
            }
            const jsons = await Promise.all(promises);
            let books: any = [];
            if (jsons && jsons.length > 0) {
                jsons.forEach((json: any) => {
                    const jsonContent = JSON.parse(json);
                    if (validateBook(jsonContent)) {
                        books.push(jsonContent)
                    }
                });
            }
            const book = mergeBook(books[0], _.drop(books));
            createFile("KiemNghichThuongKhung", "json", JSON.stringify(book));
            console.log("book:", book);
        } catch (e) {

        }
    }
};
