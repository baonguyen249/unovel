import Permissions, {RESULTS, Permission} from 'react-native-permissions'
import {
    PERMISSION_AUTHORIZED,
    PERMISSION_DENIED,
    PERMISSION_UNDETERMINED
} from "../consts/permissionConsts";
import Device from "js-kit/Device";
import {
    Linking
} from "react-native";
import {CHANGE_PERMISSIONS} from "../action-types/permissionActionTypes";
import {isValidPermissionType} from "../utils";

export function changePermissions(permissions: any) {
    return {
        type: CHANGE_PERMISSIONS,
        permissions: permissions,
    }
}

export type AppPermissionType = "notification" | "location" | "storage" | "photos" | "camera" | "contacts" | "microphone";

function transStatus(status: any) {
    if (status === RESULTS.GRANTED) {
        return PERMISSION_AUTHORIZED;
    }
    if (status === RESULTS.BLOCKED) {
        return PERMISSION_DENIED;
    }
    return PERMISSION_UNDETERMINED;
}

export function checkPermissions(permissions: AppPermissionType[]) {
    return (dispatch: any) => {
        let result: any = {
            camera: PERMISSION_UNDETERMINED,
            notification: PERMISSION_UNDETERMINED,
            contacts: PERMISSION_UNDETERMINED,
            location: PERMISSION_UNDETERMINED,
            photo: PERMISSION_UNDETERMINED,
        };
        if (permissions && Array.isArray(permissions)) {
            if (permissions.length === 1) {
                const currentPermission = permissions[0];
                if (isValidPermissionType(currentPermission)) {
                    checkPermission(currentPermission).then((status: any) => {
                        result[currentPermission] = transStatus(status);
                        dispatch(changePermissions(result));
                    });
                }
            } else if (permissions.length > 1) {
                const promises = permissions.map((value) => {
                    return checkPermission(value);
                });

                Promise.all(promises).then((_array) => {
                    if (_array.length > 0) {
                        _array.forEach((item, index) => {
                            const currentPermission = permissions[index];
                            if (isValidPermissionType(currentPermission)) {
                                result[currentPermission] = transStatus(item);
                            }
                        });
                        dispatch(changePermissions(result));
                    }
                });
            }
        }
    }
}

function checkPermission(permission: AppPermissionType) {
    return new Promise((resolve) => {
        switch (permission) {
            case "location":
                if (Device.isIos) {
                    return Permissions.check("ios.permission.LOCATION_ALWAYS").then(resolve).catch(resolve);
                } else {
                    return Permissions.check("android.permission.ACCESS_FINE_LOCATION").then(resolve).catch(resolve);
                }
            case "camera":
                if (Device.isIos) {
                    return Permissions.check("ios.permission.CAMERA").then(resolve).catch(resolve);
                } else {
                    return Permissions.check("android.permission.CAMERA").then(resolve).catch(resolve);
                }
            case "contacts":
                if (Device.isIos) {
                    return Permissions.check("ios.permission.CONTACTS").then(resolve).catch(resolve);
                } else {
                    return Permissions.check("android.permission.READ_CONTACTS").then(resolve).catch(resolve);
                }
            case "notification":
                return Permissions.checkNotifications().then((result) => {
                    if (result) {
                        const {
                            status,
                            settings,
                        } = result;
                        // console.log("checkNotifications:", {status, settings});
                        resolve(status);
                    }
                }).catch(resolve);
            case "photos":
                if (Device.isAndroid) {
                    return Permissions.check("android.permission.READ_EXTERNAL_STORAGE").then(resolve).catch(resolve);
                }
                return Permissions.check("ios.permission.PHOTO_LIBRARY").then(resolve).catch(resolve);
        }
    });
}

export function requestPermission(permission: AppPermissionType, options?: any, success?: any, error?: any) {
    return async (dispatch: any) => {
        if (permission === "notification") {
            Permissions.requestNotifications([]).then((result) => {
                if (result) {
                    const {
                        status,
                    } = result;
                    dispatch(changePermissions({
                        [permission]: transStatus(status),
                    }));
                }
                success && success(result);
            }).catch((e) => {
                dispatch(changePermissions({
                    [permission]: PERMISSION_UNDETERMINED,
                }));
                error && error(e);
            });
        } else {
            Permissions.request(transPermission(permission)).then((result) => {
                console.log("result:", result);
                let status = transStatus(result);
                dispatch(changePermissions({
                    [permission]: status,
                }));
                success && success(status);
            }).catch((e) => {
                error && error(e);
            });
        }
    }
}

function transPermission(permission: AppPermissionType): Permission {
    if (permission === "camera") {
        return Device.isIos ? "ios.permission.CAMERA" : "android.permission.CAMERA";
    }
    if (permission === "contacts") {
        return Device.isIos ? "ios.permission.CONTACTS" : "android.permission.READ_CONTACTS";
    }
    if (permission === "location") {
        return Device.isIos ? "ios.permission.LOCATION_ALWAYS" : "android.permission.ACCESS_FINE_LOCATION";
    }
    if (permission === "storage") {
        return Device.isIos ? "ios.permission.MEDIA_LIBRARY" : "android.permission.WRITE_EXTERNAL_STORAGE";
    }
    if (permission === "photos") {
        return Device.isIos ? "ios.permission.PHOTO_LIBRARY" : "android.permission.READ_EXTERNAL_STORAGE";
    }
    if (permission === "microphone") {

    }
    if (permission === "notification") {

    }
    return Device.isIos ? "ios.permission.MEDIA_LIBRARY" : "android.permission.WRITE_EXTERNAL_STORAGE";
}

export function openPermissionSettings(callback?: any) {
    return async () => {
        try {
            await Linking.openSettings();
        } catch (e) {
            console.log(e);
        } finally {
            callback && callback();
        }
    }
}

