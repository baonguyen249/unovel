import {UPDATE_CHAPTER_INFOS} from "../action-types/chapterActionTypes";
import {VariantChapter} from "../types";

export const updateChapterInfos = (chapters: VariantChapter[]) => {
    return {
        type: UPDATE_CHAPTER_INFOS,
        chapters,
    };
};
