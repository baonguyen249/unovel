import {selectCurrentAppState, selectLastBackgroundTime} from "../selectors/appSelectors";
import {AppStateStatus} from "react-native";
import {CHANGE_APP_STATE} from "../action-types/appActionTypes";
import {AppStateMap, BACKGROUND_TIME_TO_RESTART} from "../consts";
import {batch} from "react-redux";
import {showHomeRoot} from "../navigation/showRoots";
import {requestPermission} from "./permissionActions";

export const onAppStart = () => {
    return async (dispatch: any) => {
        try {
            await showHomeRoot();
            dispatch(_onAppResume());
        } catch (e) {

        }
    }
};

export const _onAppResume = () => {
    return (dispatch: any) => {
        dispatch(requestPermission("storage"));
        console.log("***_onAppResume");
    }
};

export const _onAppActive = () => {
    return (dispatch: any) => {
        console.log("***_onAppActive");
    }
};

export const _onAppHibernate = () => {
    return (dispatch: any) => {
        console.log("***_onAppHibernate");
    }
};

export const changeAppState = (nextState: AppStateStatus) => {
    const nowTime = Date.now();
    return (dispatch: any, getState: any) => {
        const state = getState();
        const lastBackgroundTime = selectLastBackgroundTime(state) || 0;
        const currentAppState = selectCurrentAppState(state);
        if (nextState !== currentAppState) {
            return;
        }
        dispatch({
            type: CHANGE_APP_STATE,
            nextState: nextState
        });
        if (nextState === AppStateMap.ACTIVE) {
            const deltaTime = nowTime - lastBackgroundTime;
            batch(() => {
                if (deltaTime >= BACKGROUND_TIME_TO_RESTART) {
                    dispatch(_onAppResume());
                }
                dispatch(_onAppActive());
            });
        } else if (nextState === AppStateMap.BACKGROUND) {
            dispatch(_onAppHibernate());
        }
    }
};
