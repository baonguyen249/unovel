import RNFS from "react-native-fs";
import {hasSuffix, isBlank} from "js-kit/utils";
import {PureBook} from "../apis/bookApi";

const docPath = RNFS.ExternalStorageDirectoryPath + "/";

export const createFile = async (name: string, ext: string, contents: string) => {
    const path = docPath + name + "." + ext;
    try {
        const files = await readFiles(ext);
        let existed = false;
        if (files && files.length > 0) {
            files.forEach((file: any) => {
                if (file) {
                    const {
                        name: _name,
                    } = file;
                    const nameExt = name + "." + ext;
                    if (nameExt === _name && !existed) {
                        existed = true;
                    }
                }
            });
        }
        if (existed) {

        }
        const result = await RNFS.writeFile(path, contents, "utf8");
        console.log("writeResult:", result);
        return Promise.resolve(result);
    } catch (e) {
        console.log(e);
        return Promise.reject(e);
    }
};

export const readFiles = async (ext?: string) => {
    try {
        const dirs = await RNFS.readDir(RNFS.ExternalStorageDirectoryPath);
        let files: any[] = [];
        if (dirs && dirs.length > 0) {
            dirs.forEach((dir: any) => {
                if (dir) {
                    const {
                        isFile,
                        isDirection,
                        name,
                    } = dir;
                    if (isFile && isFile()) {
                        if (!isBlank(ext)) {
                            if (hasSuffix("." + ext, name)) {
                                files.push(dir);
                            }
                        } else {
                            files.push(dir);
                        }
                    }
                }
            });
        }
        return Promise.resolve(files);
    } catch (e) {
        return Promise.reject(e);
    }
};

export const getJsonContent = async (path: string) => {
    /*try {
        console.log("path:", path)
        const content = await RNFS.readFile(path, "utf8");
        console.log("content:", content)
        return Promise.resolve(content);
    } catch (e) {
        console.log(e)
        return Promise.reject(e);
    }*/
    return RNFS.readFile(path);
    /* try {
         console.log("path:", path);
         const stat = await RNFS.readFile(path);
         console.log("stat:", stat);
     } catch (e) {
         console.log(e);
         return Promise.reject(e);
     }*/
};


export const validateBook = (book: any) => {
    if (book) {
        const {
            id,
            name,
            chapters,
        } = book;
        return !isBlank(id) && !isBlank(name) && chapters && chapters.length > 0;
    }
    return false;
};

export const mergeBook = (firstFragment: PureBook, fragments: PureBook[]) => {
    if (validateBook(firstFragment) && fragments && fragments.length > 0) {
        const now = Date.now();
        const book: any = {...firstFragment, chapters: firstFragment.chapters || [], createdTimeMs: now};
        fragments.forEach((frm) => {
            if (frm) {
                const {
                    chapters = [],
                } = frm;
                book.chapters.push(...chapters);
            }
        });
        console.log("book:", book);
        return book;
    }
};

export const createChapterId = (bookId: string, createdTimeMs: number, chapterNumber: number) => {
    if (bookId && bookId.length > 0 && createdTimeMs >= 0 && chapterNumber > 0) {
        return bookId + "@Version:" + createdTimeMs + "$chap:" + chapterNumber;
    }
};

export const createBookId = (bookId: string, createdTimeMs: number) => {
    if (bookId && bookId.length > 0 && createdTimeMs >= 0) {
        return bookId + "@Version:" + createdTimeMs;
    }
};
