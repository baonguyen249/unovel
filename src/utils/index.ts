import {PERMISSION_AUTHORIZED, PERMISSION_DENIED, PERMISSION_UNDETERMINED} from "../consts";

export * from "./coreUtils";

export const isValidPermissionType = (permissionType: string) => {
    return permissionType === PERMISSION_UNDETERMINED || permissionType === PERMISSION_AUTHORIZED || permissionType === PERMISSION_DENIED;
};
