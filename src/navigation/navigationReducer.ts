import {NavigationState} from "../types/clientModels";
import {Map} from "immutable";
import _ from "lodash";
import {LayoutTypes} from "./utils";
import {APPEAR_COMPONENT, CHANGE_TAB_INDEX, DISAPPEAR_COMPONENT} from "./navigationActionTypes";

interface State extends NavigationState {

}

const initState: State = {
    componentInfos: Map(),
    currentComponentId: undefined,
    currentComponentName: undefined,
    currentPageId: undefined,
    currentPageName: undefined,
    currentModalId: undefined,
    currentModalName: undefined,
    saveComponentInfos: [],
    currentBottomTabIndex: 0,
    currentOverlayId: undefined,
    currentOverlayName: undefined,
    softTabs: Map(),
};

/**
 * tools
 * */
function updateSoftTabProps(currentProps: any, nextProps: any) {
    if (!nextProps) {
        nextProps = {};
    }
    if (currentProps) {
        currentProps = {};
    }
    const {
        index,
        tabIds
    } = nextProps;
    let tabId: string | undefined;
    if (tabIds && _.isArray(tabIds) && index >= 0) {
        tabId = tabIds[index];
    }

    return {
        ...currentProps,
        ...nextProps,
        tabId,
    };
}

/**
 * handlers
 * */
function changeTabIndex(state: State, action: any) {
    const {
        index,
    } = action;
    if (index !== state.currentBottomTabIndex) {
        return {
            ...state,
            currentBottomTabIndex: index,
        }
    }
    return state;
}
function appearComponent(state: State, action: any): State {
    const {
        componentId: appearComponentId,
        componentName: appearComponentName,
        passProps,
    } = action;
    if (passProps) {
        let {
            saveComponentInfos,
            currentPageId,
            currentPageName,
            currentComponentId,
            currentModalId,
            currentModalName,
            componentInfos,
            currentOverlayId,
            currentOverlayName,
        } = state;
        const {
            navigation,
        } = passProps;

        if (appearComponentId !== currentComponentId) {
            if (navigation) {
                const {
                    savePrev,
                    layoutType,
                } = navigation;
                let nextComponentInfo = componentInfos.get(appearComponentName);
                if (!nextComponentInfo) {
                    nextComponentInfo = {};
                }
                nextComponentInfo = {
                    ...nextComponentInfo,
                    componentId: appearComponentId,
                    componentName: appearComponentName,
                    passProps: passProps,
                };

                if (layoutType === LayoutTypes.OVERLAY) {
                    const currentComponentInfo = componentInfos.get(currentPageName);
                    if (currentComponentInfo) {
                        saveComponentInfos = [
                            ...saveComponentInfos,
                            currentComponentInfo,
                        ];
                    }
                    currentOverlayId = appearComponentId;
                    currentOverlayName = appearComponentName;
                } else if (layoutType === LayoutTypes.MODAL) {
                    if (savePrev) {
                        const currentComponentInfo = componentInfos.get(currentPageName);
                        if (currentComponentInfo) {
                            saveComponentInfos = [
                                ...saveComponentInfos,
                                currentComponentInfo,
                            ];
                        }
                    }
                    currentModalId = appearComponentId;
                    currentModalName = appearComponentName;
                } else if (layoutType === LayoutTypes.STACK || layoutType === LayoutTypes.BOTTOM_TAB) {
                    currentPageName = appearComponentName;
                    currentPageId = appearComponentId;
                }
                componentInfos = componentInfos.set(appearComponentName, nextComponentInfo);
            }
            return {
                ...state,
                componentInfos,
                currentComponentName: appearComponentName,
                currentComponentId: appearComponentId,
                currentPageName,
                currentPageId,
                currentModalId,
                currentModalName,
                saveComponentInfos,
                currentOverlayId,
                currentOverlayName,
            };
        }
    }
    return state;
}

function disappearComponent(state: State, action: any): State {
    const {
        componentName: disappearComponentName,
    } = action;
    let {
        componentInfos,
    } = state;

    let componentInfo = componentInfos.get(disappearComponentName);
    if (componentInfo) {
        const {
            passProps,
        } = componentInfo;
        if (passProps) {
            const {
                navigation,
            } = passProps;
            if (navigation) {
                const {
                    layoutType,
                    savePrev,
                    lastPageName,
                } = navigation;
                if (lastPageName) {
                    const lastPageInfo = componentInfos.get(lastPageName);
                    if (lastPageInfo) {
                        const {
                            componentId,
                        } = lastPageInfo;
                        return {
                            ...state,
                            saveComponentInfos: [],
                            currentPageId: componentId,
                            currentPageName: lastPageName,
                            currentComponentId: componentId,
                            currentComponentName: lastPageName,
                        };
                    }
                }

                if (layoutType === LayoutTypes.OVERLAY || (layoutType === LayoutTypes.MODAL && savePrev)) {
                    let {
                        saveComponentInfos,
                        currentComponentName,
                        currentComponentId,
                        currentPageId,
                        currentPageName,
                    } = state;
                    const saveComponentInfo = _.last(saveComponentInfos);
                    if (saveComponentInfo) {
                        const {
                            componentId,
                            componentName,
                            passProps,
                        } = saveComponentInfo;
                        if (passProps) {
                            const {
                                navigation,
                            } = passProps;
                            if (navigation) {
                                const {
                                    layoutType,
                                } = navigation;
                                if (layoutType === LayoutTypes.STACK || layoutType === LayoutTypes.BOTTOM_TAB) {
                                    currentPageId = componentId;
                                    currentPageName = componentName;
                                }
                                currentComponentId = componentId;
                                currentComponentName = componentName;
                                saveComponentInfos = _.dropRight(saveComponentInfos);
                                return {
                                    ...state,
                                    saveComponentInfos,
                                    currentPageId,
                                    currentPageName,
                                    currentComponentId,
                                    currentComponentName,
                                };
                            }
                        }
                    }
                }
            }
        }
    }
    return state;
}

function updateSoftTabs(state: State, action: any): State {
    const {
        componentId,
        passProps,
    } = action;
    if (componentId && passProps) {
        const {
            index,
            tabIds,
        } = passProps;
        let {
            softTabs,
        } = state;
        let currentSoftTabs = softTabs.get(componentId);
        currentSoftTabs = updateSoftTabProps(currentSoftTabs, passProps);
        softTabs = softTabs.set(componentId, currentSoftTabs);
        return {
            ...state,
            softTabs,
        };
    }
    return state;
}
export default function (state: State = initState, action: any) {
    switch (action.type) {
        case CHANGE_TAB_INDEX:
            return changeTabIndex(state, action);
        case APPEAR_COMPONENT:
            return appearComponent(state, action);
        case DISAPPEAR_COMPONENT:
            return disappearComponent(state, action);
    }
    return state;
}
