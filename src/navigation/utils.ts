export enum LayoutTypes {
    STACK,
    MODAL,
    OVERLAY,
    BOTTOM_TAB,
}

export function mergePushProps(passProps: any) {
    if (!passProps) {
        passProps = {};
    }
    return {
        ...passProps,
        navigation: {
            layoutType: LayoutTypes.STACK,
        },
    };
}

export function mergeShowModalProps(passProps: any, savePrev = false) {
    if (!passProps) {
        passProps = {};
    }
    return {
        ...passProps,
        navigation: {
            layoutType: LayoutTypes.MODAL,
            savePrev: savePrev,
        },
    };
}

export function mergeShowOverlayProps(passProps: any, navigationProps?: any) {
    if (!passProps) {
        passProps = {};
    }
    if (!navigationProps) {
        navigationProps = {};
    }
    return {
        ...passProps,
        navigation: {
            layoutType: LayoutTypes.OVERLAY,
            ...navigationProps,
        },
    };
}


export function mergeBottomTabProps(passProps: any) {
    if (!passProps) {
        passProps = {};
    }
    return {
        ...passProps,
        navigation: {
            layoutType: LayoutTypes.BOTTOM_TAB,
        },
    };
}

export function getNavigationProps(passProps: any) {
    if (passProps && passProps.passProps) {
        const {
            navigation,
        } = passProps.passProps;
        return navigation;
    }
}

export function getLayoutType(passProps: any) {
    const navigationProps = getNavigationProps(passProps);
    if (navigationProps) {
        const {
            layoutType,
        } = navigationProps;
        return layoutType;
    }
}

