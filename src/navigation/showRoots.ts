import {Navigation} from "react-native-navigation";
import {HOME_SCREEN_RN} from "./routeNames";
import {mergePushProps} from "./utils";
import {navSetDefaultOptions} from "native-kit/navigation";
import {primaryColors} from "../consts/colors";
import Device from "js-kit/Device";
function setDefaultOptions() {
    navSetDefaultOptions({
        popGesture: true,
        topBar: {
            visible: false,
            animate: false, // Controls whether TopBar visibility changes should be animated
            drawBehind: true,
        },
        layout: {
            /**
             *
             * */
            orientation: (!Device.isTablet && Device.isAndroid) ? ["portrait"] : ["landscape", "portrait"], // phone lock to portrait
        },
        statusBar: {
            drawBehind: false,
            visible: true,
            backgroundColor: primaryColors.lightTheme.regular,
            style: "light",
        },
        bottomTabs: {
            visible: false,
            drawBehind: true,
            animate: false,
        }
    });
}
export const showHomeRoot = async () => {
    try {
        console.log("showHomeRoot");
        setDefaultOptions();
        await Navigation.setRoot({
            root: {
                stack: {
                    children: [
                        {
                            component: {
                                name: HOME_SCREEN_RN,
                                passProps: mergePushProps({}),
                            }
                        }
                    ],
                }
            }
        });
    } catch (e) {

    }
};
