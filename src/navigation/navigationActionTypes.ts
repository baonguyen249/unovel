const addPrefix = (type: string) => {
    return "NAVIGATION_ACTIONS/" + type;
};
export const APPEAR_COMPONENT = addPrefix("APPEAR_COMPONENT");
export const DISAPPEAR_COMPONENT = addPrefix("DISAPPEAR_COMPONENT");
export const CHANGE_TAB_INDEX = addPrefix("CHANGE_TAB_INDEX");
