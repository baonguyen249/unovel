import {APPEAR_COMPONENT, CHANGE_TAB_INDEX, DISAPPEAR_COMPONENT} from "./navigationActionTypes";
import {BOTTOM_TABS_ID} from "./routeIds";
import {primaryColors} from "../consts";
import {Navigation, Options} from "react-native-navigation";
import {ChapterListProps, PushStackProps} from "../types";
import {selectComponentInfos, selectCurrentPageId, selectCurrentPageName} from "../selectors/navigationSelectors";
import {CHAPTER_LIST_SCREEN_RN, CHAPTER_SCREEN_RN, LIBRARY_SCREEN_RN} from "./routeNames";
import {mergePushProps} from "./utils";
import {navMergeOptions, navPop, navPush} from "native-kit/navigation";
import {isBlank} from "js-kit/utils";

export function appearComponent(params: { componentId?: string, componentName?: string, passProps?: any }) {
    return (dispatch: any) => {
        if (!params) {
            return;
        }
        const {
            componentId,
            componentName,
            passProps,
        } = params;
        dispatch({
            type: APPEAR_COMPONENT,
            componentId,
            componentName,
            passProps,
        });
    }
}

export function disappearComponent(params: { componentId?: string, componentName?: string }) {
    return (dispatch: any) => {
        if (!params) {
            return;
        }
        const {
            componentId,
            componentName,
        } = params;
        dispatch({
            type: DISAPPEAR_COMPONENT,
            componentId,
            componentName,
        });
    }
}

export function changeTabIndex(nextIndex: number) {
    navMergeOptions(BOTTOM_TABS_ID, {
        bottomTabs: {
            currentTabIndex: nextIndex,
        }
    });
    return {
        type: CHANGE_TAB_INDEX,
        nextIndex: nextIndex || 0, // nextIndex === (undefined || null || 0)  => return 0
    };
}

/**
 * Common Push Stack
 * */
async function pushStack(params: PushStackProps) {
    if (params) {
        let {
            dispatch,
            getState,
            currentComponentId,
            nextComponentName,
            passProps,
        } = params;
        const state = getState();
        let options: Options = params.options;
        if (options) {
            options = {};
        }
        options = {
            ...options,
            statusBar: {
                backgroundColor: primaryColors.lightTheme.regular,
                style: "light",
            },
            layout: {
                backgroundColor: "white",
            },
            animations: {
                push: {
                    enabled: true,
                    waitForRender: true,
                }
            },
        };
        const currentPageId = selectCurrentPageId(state) || "";
        const canPush = !isBlank(currentPageId) || !isBlank(currentComponentId);
        try {
            if (canPush) {
                const result = await navPush(currentComponentId || currentPageId, nextComponentName, passProps, options);
                return Promise.resolve(result);
            }
            return Promise.resolve();
        } catch (e) {
            return Promise.reject(e);
        }
    }
    return Promise.reject();
}

/**
 * Common Pop Stack
 * */
export function popStack(success?: any): any {
    return async (dispatch: any, getState: any) => {
        const {
            navigationState,
        }: any = getState();
        const {
            currentPageId,
        } = navigationState;
        console.log("popStack:", {currentPageId});
        try {
            currentPageId && await navPop(currentPageId);
            success && success();
        } catch (e) {
            console.log(e);
        }
    }
}

interface CommonPushStackProps {
    nextComponentName: string,
    passProps?: any,
    onSuccess?: any,
    onError?: any,
    onFinal?: any,
}

const _commonPushStack = (props: CommonPushStackProps) => {
    return async (dispatch: any, getState: any) => {
        if (!props) {
            return;
        }
        const {
            passProps,
            nextComponentName,
        } = props;
        const state = getState();
        const currentPageId = selectCurrentPageId(state);
        try {
            await pushStack({
                dispatch: dispatch,
                getState: getState,
                nextComponentName,
                currentComponentId: currentPageId,
                passProps: mergePushProps(passProps),
                options: {},
            });
        } catch (e) {
            console.log(e);
        }
    };
};

/**
 * chapter
 * */
interface OpenChapter {
    chapterId: string,
}

export const openChapter = (params: OpenChapter) => {
    return async (dispatch: any, getState: any) => {
        if (!params) {
            return;
        }
        const state = getState();
        const {
            chapterId,
        } = params;
        if (chapterId) {
            const currentPageId = selectCurrentPageId(state);
            try {
                await pushStack({
                    dispatch: dispatch,
                    getState: getState,
                    nextComponentName: CHAPTER_SCREEN_RN,
                    currentComponentId: currentPageId,
                    passProps: mergePushProps(params),
                    options: {},
                });
            } catch (e) {
                console.log(e);
            }
        }
    }
};

export const openChapterList = ({bookId, onChapterPress}: ChapterListProps) => {
    return async (dispatch: any, getState: any) => {
        try {
            const state = getState();
            const currentPageId = selectCurrentPageId(state);
            await pushStack({
                dispatch: dispatch,
                getState: getState,
                nextComponentName: CHAPTER_LIST_SCREEN_RN,
                currentComponentId: currentPageId,
                passProps: mergePushProps({bookId, onChapterPress}),
                options: {},
            });
        } catch (e) {

        }
    }
};

export const openLibrary = () => {
    return  (dispatch: any, getState: any) => {
        dispatch(_commonPushStack({
            nextComponentName: LIBRARY_SCREEN_RN,
            passProps: {},
        }));
    }
};
