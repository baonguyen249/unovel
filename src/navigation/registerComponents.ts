import {Navigation} from "react-native-navigation";
import {LIBRARY_SCREEN_RN, CHAPTER_LIST_SCREEN_RN, CHAPTER_SCREEN_RN, HOME_SCREEN_RN, SETTINGS_SCREEN_RN} from "./routeNames";
import {gestureHandlerRootHOC} from "react-native-gesture-handler";
import HomePage from "../gui/pages/HomePage";
import {Provider} from "react-redux";
import reduxStore from "../reduxStore";
import LibraryPage from "../gui/pages/LibraryPage";
import ChapterPage from "../gui/pages/ChapterPage";
import ChapterListPage from "../gui/pages/ChapterListPage";
import SettingsPage from "../gui/pages/SettingsPage";

const _wrapRedux = (componentName: string, componentProvider: () => any) => {
    Navigation.registerComponentWithRedux(componentName, componentProvider, Provider, reduxStore);
};
const registerComponents = () => {
    _wrapRedux(HOME_SCREEN_RN, () => gestureHandlerRootHOC(HomePage));
    _wrapRedux(LIBRARY_SCREEN_RN, () => gestureHandlerRootHOC(LibraryPage));
    _wrapRedux(CHAPTER_SCREEN_RN, () => gestureHandlerRootHOC(ChapterPage));
    _wrapRedux(CHAPTER_LIST_SCREEN_RN, () => gestureHandlerRootHOC(ChapterListPage));
    _wrapRedux(SETTINGS_SCREEN_RN, () => gestureHandlerRootHOC(SettingsPage));
};

export default registerComponents;
