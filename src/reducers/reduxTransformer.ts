import {createTransform} from "redux-persist";
import {BOOK_STATE_KEY, CHAPTER_STATE_KEY} from "../consts/stateKeys";
import {
    toJs as bookStateToJs,
    fromJs as bookStateFromJs
} from "./bookReducer";
import {
    toJs as chapterStateToJs,
    fromJs as chapterStateFromJs,
} from "./chapterReducer";

function toJs(inboundState: any, key: string | number) {
    switch (key) {
        case BOOK_STATE_KEY:
            return bookStateToJs(inboundState);
        case CHAPTER_STATE_KEY:
            return chapterStateToJs(inboundState);
    }
    return inboundState;
}

function fromJs(outboundState: any, key: string | number) {
    switch (key) {
        case BOOK_STATE_KEY:
            return bookStateFromJs(outboundState);
        case CHAPTER_STATE_KEY:
            return chapterStateFromJs(outboundState);
    }
    return outboundState;
}

const reduxTransformer = createTransform(toJs, fromJs);

export default reduxTransformer;
