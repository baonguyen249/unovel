import {ChapterState} from "../types";
import {Map} from "immutable";
import {
    DELETE_CHAPTER_INFOS,
    UPDATE_CHAPTER_INFOS,
} from "../action-types/chapterActionTypes";
import {PureChapter} from "../apis/bookApi";
import {createChapterId} from "../utils";

interface State extends ChapterState {

}

const initState: State = {
    chapterInfos: Map(),
};

const _updateChapterInfos = (state: State, action: any): State => {
    const {
        chapters,
    } = action;
    if (chapters && chapters.length > 0) {
        let {
            chapterInfos,
        } = state;
        chapters.forEach((chapter: PureChapter) => {
            const {
                id,
                chapterNumber = 0,
                createdTimeMs = 0,
                bookId = "",
            } = chapter;
            const chapterId = createChapterId(bookId, createdTimeMs, chapterNumber);
            let currentChapterInfo = chapterInfos.get(id);
            if (!currentChapterInfo) {
                currentChapterInfo = {};
            }
            currentChapterInfo = {
                ...currentChapterInfo,
                ...chapter,
            };
            chapterInfos = chapterInfos.set(id, currentChapterInfo);
        });
        return {
            ...state,
            chapterInfos,
        };
    }
    return state;
};

const _deleteChapterInfos = (state: State, action: any): State => {
    return state;
};

const chapterReducer = (state = initState, action: any): State => {
    if (action) {
        switch (action.type) {
            case UPDATE_CHAPTER_INFOS:
                return _updateChapterInfos(state, action);
            case DELETE_CHAPTER_INFOS:
                return _deleteChapterInfos(state, action);
        }
    }
    return state;
};

export default chapterReducer;

export const toJs = (state: State): any => {
    if (state) {
        const {
            chapterInfos,
        } = state;
        return {
            chapterInfos: chapterInfos.toObject(),
        }
    }
};

export const fromJs = (state: any): State => {
    const {
        chapterInfos
    } = state;
    return {
        chapterInfos: Map(chapterInfos),
    };
};
