import {AppState} from "../types/clientModels";
import {CHANGE_APP_STATE} from "../action-types/appActionTypes";

interface State extends AppState {

}

const initState: State = {
    lastBackgroundTime: 0,
    lastActiveTime: 0,
    currentAppState: undefined,
};

const _changeAppState = (state: State, action: any): State => {
    return state;
};

const appReducer = (state = initState, action: any): State => {
    if (action) {
        switch (action.type) {
            case CHANGE_APP_STATE:
                return _changeAppState(state, action);

        }
    }
    return state;
};
export default appReducer;
