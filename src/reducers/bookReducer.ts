import {BookState} from "../types";
import {Map} from "immutable";
import {DELETE_BOOK_INFOS, UPDATE_BOOK_INFOS} from "../action-types/bookActionTypes";
import {Book} from "../types";

// c***: current***
interface State extends BookState {

}


const initState: State = {
    bookInfos: Map(),
};

const _updateBookInfos = (state: State, action: any): State => {
    const {
        books: uBooks,
    } = action;
    if (uBooks && uBooks.length > 0) {
        let {
            bookInfos,
        } = state;
        uBooks.forEach((uBook: Book) => {
            if (uBook) {
                const {
                    id,
                } = uBook;
                let cBookInfo = bookInfos.get(id);
                if (!cBookInfo) {
                    cBookInfo = {};
                }
                cBookInfo = {
                    ...cBookInfo,
                    ...uBook,
                };
                bookInfos = bookInfos.set(id, cBookInfo);
            }
        });
        return {
            ...state,
            bookInfos,
        };
    }
    return state;
};

const _deleteBookInfos = (state: State, action: any): State => {
    return state;
};

const bookReducer = (state = initState, action: any): State => {
    if (action) {
        switch (action.type) {
            case UPDATE_BOOK_INFOS:
                return _updateBookInfos(state, action);
            case DELETE_BOOK_INFOS:
                return _deleteBookInfos(state, action);
        }
    }
    return state;
};

export default bookReducer;

export const toJs = (state: State): any => {
    if (state) {
        const {
            bookInfos,
        } = state;
        return {
            bookInfos: bookInfos.toObject(),
        }
    }
};

export const fromJs = (state: any): State => {
    const {
        bookInfos
    } = state;
    return {
        bookInfos: Map(bookInfos),
    };
};
