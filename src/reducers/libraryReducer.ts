import {LibraryState} from "../types";
import {Map} from "immutable";
import {UPDATE_BOOKS_TO_LIBRARY} from "../action-types/libraryActionTypes";

interface State extends LibraryState {

}

const initState: State = {
    books: Map(),
};

const _updateBooks = (state: State, action: any): State => {
    const {
        books: nBooks = [],
        refreshed,
    } = action;
    if (nBooks.length > 0) {
        let {
            books: cBooks,
        } = state;
        nBooks.forEach((nBook: any) => {
            if (nBook) {
                const {
                    uniqueId,
                } = nBook;
                let cBook = cBooks.get(uniqueId);
                if (!cBook) {
                    cBook = {};
                }
                cBook = {
                    ...cBook,
                    ...nBook,
                    updatedTimeMs: Date.now(),
                };
                cBooks = cBooks.set(uniqueId, cBook);
            }
        });
        return {
            ...state,
            books: cBooks,
        };
    }
    return state;
};

export default function libraryReducer(state = initState, action: any): State {
    if (action) {
        switch (action.type) {
            case UPDATE_BOOKS_TO_LIBRARY:
                return _updateBooks(state, action);
        }
    }
    return state;
}
