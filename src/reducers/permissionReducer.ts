import {
    CHANGE_PERMISSIONS
} from '../action-types/permissionActionTypes';
import {PermissionState} from "../types/clientModels";

type State = PermissionState;
const initState: State = {
    contacts: undefined,
    location: undefined,
    camera: undefined,
    notification: undefined,
    microphone: undefined,
    photos: undefined,
    storage: undefined,
};

function changePermissions(state: State, action: any): State {
    const {
        permissions
    } = action;
    if (permissions) {
        return {
            ...state,
            ...permissions
        }
    }
    return state;
}

export default function (state: State = initState, action: any): State {
    switch (action.type) {
        case CHANGE_PERMISSIONS:
            return changePermissions(state, action);
    }
    return state;
}

export function toJs(state: State): any {
    let {
        contacts,
        location,
        camera,
        photos,
        notification,
        microphone,
    } = state;
    return {
        contacts,
        location,
        camera,
        photos,
        notification,
        microphone,
    };
}

export function fromJs(state: any): State {
    const {
        contacts,
        location,
        camera,
        photos,
        notification,
        microphone,
    } = state;
    return {
        ...initState,
        contacts,
        location,
        camera,
        photos,
        notification,
        microphone,
    }
}
