//@ts-ignore
import cheerio from "react-native-cheerio";
import _ from "lodash";
import {delPrefix, hasPrefix} from "js-kit/utils";

export interface PureBook {
    id?: string,
    name?: string,
    description?: string,
    author?: PureAuthor,
    chapters?: PureChapter[],
}

export interface PureChapter {
    id?: string,
    bookId?: string,
    title?: string,
    body?: string,
    createdTimeMs?: number,
    updatedTimeMs?: number,
    nextId?: string,
    prevId?: string,
    chapterNumber?: number,
}

interface PureAuthor {
    id?: string,
    name?: string,
    avatarUrl?: string,
    numberOfBooks?: number,
}

const BASE_URL = "https://truyen.tangthuvien.vn/doc-truyen/";
const BASE_AUTHOR_URL = "https://truyen.tangthuvien.vn/tac-gia?author=";
const CHAPTER_ID_PREFIX = ":chapter";
const AUTHOR_ID_PREFIX = "author";

const _createChapterId = (bookId: string, chapterNumber: number) => {
    if (bookId && chapterNumber > 0) {
        return bookId + CHAPTER_ID_PREFIX + chapterNumber;
    }
    return "";
};

const _createAuthorId = (id: string | number) => {
    return AUTHOR_ID_PREFIX + id;
};

const _fetchingData = async (url: string, options?: { begin: number, end: number, total: number }): Promise<PureBook | undefined> => {
    try {
        const bookId = delPrefix(BASE_URL, url);
        let chapters: PureChapter[] = [];

        const response = await fetch(url);
        const htmlString = await response.text();
        // console.log("htmlString:", htmlString)
        const $ = cheerio.load(htmlString);
        // const body = $(".box-chap").text();
        // console.log("body:", body);

        const bookName = $("div.book-info h1").text();

        const authorUrl = $("div.book-info p.tag a.blue").attr("href");
        const author = await _getAuthor(authorUrl);

        const description = $("div.book-intro p").text();
        let totalChaptersString = $(".book-state ul li.update div.detail").text();
        let totalChaptersNumber = 0;
        if (totalChaptersString && totalChaptersString.length > 0) {
            totalChaptersString = _.trim(totalChaptersString);
            totalChaptersNumber = totalChaptersString.match(/(\d+)/)[0];
        }
        console.log("totalChaptersNumber:", totalChaptersNumber);
        if (totalChaptersNumber > 0) {
            const chapterBaseUrl = url + "/chuong-";

            // START - Test
            /*const tempChapters: PureChapter[] = [];
            await _getChapters(chapterBaseUrl, 1, 3, totalChaptersNumber, tempChapters);
            const _fetchText = async () => {
                const response = await fetch(chapterBaseUrl + 1 + "");
                console.log("response:", response);
                const htmlString = await response.text();
                const $ = cheerio.load(htmlString);
                const body = $(".box-chap").text();
                const title = $(".more-chap.btn").text();
                return Promise.resolve({body, title, bookId});
            };
            const result = await _fetchText();*/
            // END - Test
            let begin = 1;
            let end = 3;
            let total = totalChaptersNumber; // MAXIMUM
            if (options) {
                begin = options.begin;
                end = options.end;
                total = options.total;
            }

            await _getChapters(chapterBaseUrl, begin, end, total, chapters, bookId);
        }
        const currentBook: PureBook = {
            id: bookId,
            name: bookName,
            author: author,
            description,
            chapters,
        };
        return Promise.resolve(currentBook);
    } catch (e) {
        return Promise.reject(e);
    }
};

const _getAuthor = async (url: string) => {
    if (url && hasPrefix(BASE_AUTHOR_URL, url)) {
        const id = _createAuthorId(delPrefix(BASE_AUTHOR_URL, url));

        // START - fetchAuthorInfo
        const response = await fetch(url);
        const htmlString = await response.text();
        // console.log("htmlString:", htmlString);
        const $ = cheerio.load(htmlString);
        let name = $("div.info-wrap.nobt div#authorId.author-photo p").text();
        if (name) {
            name = _.trim(name);
        }
        const avatarUrl = $("div #authorId.author-photo a img").attr("src");
        const ems = $("div.info-wrap li em").contents();
        let numberOfBooks = 0;
        if (ems && ems[0]) {
            const data = ems[0].data || "";
            const num = _.toInteger(data);
            if (num > 0) {
                numberOfBooks = num;
            }
        }
        const author: PureAuthor = {
            id,
            name,
            avatarUrl,
            numberOfBooks,
        };
        return Promise.resolve(author);
    }
};

const _getChapters = async (baseUrl: string, begin: number, end: number, total: number, tempChapters: PureChapter[], bookId: string) => {
    let promiseArr: any[] = [];
    for (let i = begin; i <= end; i++) {
        const _fetchText = async () => {
            const response = await fetch(baseUrl + i + "");
            const htmlString = await response.text();
            const $ = cheerio.load(htmlString);
            const id = _createChapterId(bookId, i);
            let nextId: string | undefined;
            let prevId: string | undefined;
            if (i + 1 <= total) {
                nextId = _createChapterId(bookId, i + 1);
            }
            if (i - 1 > 0) {
                prevId = _createChapterId(bookId, i - 1);
            }
            const body = $(".box-chap").text();
            const title = $(".more-chap.btn").text();
            const createdTimeMs = Date.now();
            const chapter: PureChapter = {
                createdTimeMs,
                body,
                title,
                bookId,
                id,
                nextId,
                prevId,
                chapterNumber: i,
            };
            return Promise.resolve(chapter);
        };
        promiseArr.push(_fetchText());
    }
    const results = await Promise.all(promiseArr);
    console.log("results begin " + begin + ":", results);
    tempChapters.push(...results);
    if (end < total) {
        const delta = end - begin;
        begin = end + 1;
        end = begin + delta;
        if (end > total) {
            end = total;
        }
        const clock = (ms = 0) => {
            return new Promise((resolve) => {
                setTimeout(() => {
                    resolve();
                }, ms);
            });
        };
        await clock(2000);
        const a: any = await _getChapters(baseUrl, begin, end, total, tempChapters, bookId);
        return Promise.resolve(a);
    } else if (end === total) {
        return Promise.resolve(tempChapters);
    }
    return Promise.reject();
};

export const getBookFromTTV = async (url: string) => {
    try {
        if (hasPrefix(BASE_URL, url)) {
            const result = await _fetchingData(url, {begin: 1501, end: 1504, total: 1896});
            return Promise.resolve(result);
        }
    } catch (e) {
        console.log(e);
    } finally {

    }
};
