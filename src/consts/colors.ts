import {PrimaryColors, SecondaryColors} from "../theme/themeTypes";

export const primaryColors = {
    lightTheme: {
        light: "",
        regular: "",
        dark: "",
    }
};
export const secondaryColors = {
    lightTheme: {
        light: "",
        regular: "",
        dark: "",
    }
};

export const borderColors = {
    darkTheme: {
        light: "#414141",
        regular: "#373737",
        dark: "#2d2d2d",
    },
    lightTheme: {
        light: "#ebebeb",
        regular: "#d7d7d7",
        dark: "#c3c3c3",
    },
};
