import {FontSizes, FontWeights} from "../theme/themeTypes";
import {
    BODY_FONT_SIZE,
    CAPTION_FONT_SIZE,
    DESCRIPTION_FONT_SIZE,
    H1_FONT_SIZE,
    TITLE_FONT_SIZE,
    H2_FONT_SIZE,
    H3_FONT_SIZE,
    H4_FONT_SIZE,
    H5_FONT_SIZE,
    H6_FONT_SIZE,
} from "./dimens";

const TITLE_WEIGHT = "600";
const BODY_WEIGHT = "400";
const DESCRIPTION_WEIGHT = "500";
const CAPTION_WEIGHT = "300";
const H_WEIGHT = "700";

export const fontWeights: FontWeights = {
    title: TITLE_WEIGHT,
    body: BODY_WEIGHT,
    description: DESCRIPTION_WEIGHT,
    caption: CAPTION_WEIGHT,
    h1: H_WEIGHT,
    h2: H_WEIGHT,
    h3: H_WEIGHT,
    h4: H_WEIGHT,
    h5: H_WEIGHT,
    h6: H_WEIGHT,
};

export const fontSizes: FontSizes = {
    title: TITLE_FONT_SIZE,
    body: BODY_FONT_SIZE,
    description: DESCRIPTION_FONT_SIZE,
    caption: CAPTION_FONT_SIZE,
    h1: H1_FONT_SIZE,
    h2: H2_FONT_SIZE,
    h3: H3_FONT_SIZE,
    h4: H4_FONT_SIZE,
    h5: H5_FONT_SIZE,
    h6: H6_FONT_SIZE,
};
