export const TITLE_FONT_SIZE = 22;
export const BODY_FONT_SIZE = 16;
export const DESCRIPTION_FONT_SIZE = 14;
export const CAPTION_FONT_SIZE = 12;
export const H1_FONT_SIZE = 28;
export const H2_FONT_SIZE = 26;
export const H3_FONT_SIZE = 24;
export const H4_FONT_SIZE = 22;
export const H5_FONT_SIZE = 20;
export const H6_FONT_SIZE = 18;

export const STATUS_BAR_HEIGHT = 20;
export const TOP_BAR_HEIGHT = 44;
