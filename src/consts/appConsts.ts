import {AppStateMapT} from "../types/coreModels";
import {TIME_MINUTE, TIME_SECOND} from "js-kit/utils";

export const AppStateMap: AppStateMapT = {
    ACTIVE: "active",
    BACKGROUND: "background",
};

export const BACKGROUND_TIME_TO_RESTART = __DEV__ ? 5 * TIME_SECOND : 5 * TIME_MINUTE;
