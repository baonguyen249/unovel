export const APP_STATE_KEY = "appState";
export const NAVIGATION_STATE_KEY = "navigationState";
export const PERMISSION_STATE_KEY = "permissionState";
export const BOOK_STATE_KEY = "bookState";
export const CHAPTER_STATE_KEY  = "chapterState";
export const LIBRARY_STATE_KEY = "libraryState";
