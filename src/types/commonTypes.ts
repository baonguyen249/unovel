import React from "react";

export type ItemComponentType<P> = { regNums: number[], Component: React.ComponentType<P> };
export type PermissionKey = "camera" | "photos" | "contacts" | "location" | "microphone" | "notification" | "storage";

