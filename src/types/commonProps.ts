import React from "react";
import {ViewStyleType} from "./styleTypes";
import {ItemComponentType} from "./commonTypes";

export interface ComponentProps<T = ViewStyleType, C = any> {
    style?: T,
    children?: C,
}

export interface ReduxComponentProps<G, D, T = ViewStyleType> extends ComponentProps<T> {
    getStateProps?: G,
    dispatchProps?: D,
}

export interface ScreenProps<G, D> extends ReduxComponentProps<G, D> {
    componentId: string,
    navigation: any,
}

export interface PushStackProps {
    dispatch: any,
    getState: any,
    currentComponentId?: string,
    nextComponentName: string,
    passProps?: any,
    options?: any,
}

export interface ChapterItemProps {
    item: any,
    index: number,
    onPress?: any,
    regNum: number,
}

export interface ChapterItemWrapperProps extends ChapterItemProps {
    itemComponents: ItemComponentType<ChapterItemProps>[],
}

export interface ChapterListProps {
    bookId: string,
    onChapterPress?: any,
}
