import {AppStateStatus} from "react-native";

export type AppStateMapT = {
    ACTIVE: AppStateStatus,
    BACKGROUND: AppStateStatus,
};

export interface Book {
    uniqueId: string,
    id?: string,
    name?: string,
    author?: any,
    description?: string,
    chapters: any[],
    updatedTimeMs?: number,
}

export interface Chapter {
    uniqueId: string,
    id?: string,
    title?: string,
    body?: string,
    chapterNumber?: number,
    createdTimeMs?: number,
}

export interface VariantBook extends VariantObject<Book> {

}

export interface VariantChapter extends VariantObject<Chapter> {

}

interface VariantObject<V> {
    id: string,
    versions?: {
        [key: string]: V,
    }
}