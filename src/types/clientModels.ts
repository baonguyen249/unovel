import {AppStateStatus} from "react-native";
import {PermissionKey} from "./commonTypes";

export interface AppState {
    currentAppState?: AppStateStatus,
    lastActiveTime: number,
    lastBackgroundTime: number,
}

export type PermissionState<T = any> = {
    [key in PermissionKey]?: T
};

export interface NavigationState {
    componentInfos: any,
    currentComponentId?: string,
    currentComponentName?: string,
    currentPageId?: string,
    currentPageName?: string,
    currentModalId?: string,
    currentModalName?: string,
    saveComponentInfos: any[],
    currentBottomTabIndex: number,
    currentOverlayId?: string,
    currentOverlayName?: string,
    softTabs: any,
}

export interface BookState {
    bookInfos?: any,
}
export interface ChapterState {
    chapterInfos?: any,
}
export interface LibraryState {
    books?: any,
}
